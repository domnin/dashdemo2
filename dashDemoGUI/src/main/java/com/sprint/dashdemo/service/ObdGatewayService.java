package com.sprint.dashdemo.service;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.google.inject.Inject;
import com.sprint.dashdemo.gui.activity.ConfigActivity;
import com.sprint.dashdemo.gui.util.Helper;
import com.sprint.dashdemo.tasks.ObdTimerTask;
import com.sprint.obd.Config;
import com.sprint.obd.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

import pt.lighthouselabs.obd.commands.ObdCommand;
import pt.lighthouselabs.obd.reader.ObdProgressListener;
import pt.lighthouselabs.obd.reader.io.AbstractGatewayService;
import pt.lighthouselabs.obd.reader.io.ObdCommandJob;
import pt.lighthouselabs.obd.reader.io.ObdCommandJob.ObdCommandJobState;
import pt.lighthouselabs.obd.reader.util.ObdHelper;

/**
 * This service is primarily responsible for establishing and maintaining a
 * permanent connection between the device where the application runs and a more
 * OBD Bluetooth interface.
 * <p/>
 * Secondarily, it will serve as a repository of ObdCommandJobs and at the same
 * time the application state-machine.
 */
public class ObdGatewayService extends AbstractGatewayService {
    public static boolean isConnected = false;
    private static final String TAG = ObdGatewayService.class.getName();
    /*
     * http://developer.android.com/reference/android/bluetooth/BluetoothDevice.html
     * #createRfcommSocketToServiceRecord(java.util.UUID)
     *
     * "Hint: If you are connecting to a Bluetooth serial board then try using the
     * well-known SPP UUID 00001101-0000-1000-8000-00805F9B34FB. However if you
     * are connecting to an Android peer then please generate your own unique
     * UUID."
     */
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private final IBinder binder = new ObdGatewayServiceBinder();
    @Inject
    SharedPreferences prefs;

    Config config;
    private BluetoothDevice dev = null;
    private BluetoothSocket sock = null;
    private BluetoothSocket sockFallback = null;

    final List<Timer> connectionMonitorWorker = new ArrayList<Timer>();

    public void startService() {
        Log.d(TAG, "Starting service..");
        config = new Config(prefs);

        // get the remote Bluetooth device
        final String remoteDevice = prefs.getString(Constants.BLUETOOTH_LIST_KEY, null);
        if (remoteDevice == null || "".equals(remoteDevice)) {
            Toast.makeText(ctx, "No Bluetooth device selected", Toast.LENGTH_LONG).show();

            // log error
            Log.e(TAG, "No Bluetooth device has been selected.");

            // TODO kill this service gracefully
            Intent configIntent = new Intent(this, ConfigActivity.class) ;
            configIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(configIntent);
            stopService();

            return;
        }

        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        dev = btAdapter.getRemoteDevice(remoteDevice);

    /*
     * TODO clean
     *
     * Get more preferences
     */

        ArrayList<ObdCommand> cmds = config.getObdCommands();

    /*
     * Establish Bluetooth connection
     *
     * Because discovery is a heavyweight procedure for the Bluetooth adapter,
     * this method should always be called before attempting to connect to a
     * remote device with connect(). Discovery is not managed by the Activity,
     * but is run as a system service, so an application should always call
     * cancel discovery even if it did not directly request a discovery, just to
     * be sure. If Bluetooth state is not STATE_ON, this API will return false.
     *
     * see
     * http://developer.android.com/reference/android/bluetooth/BluetoothAdapter
     * .html#cancelDiscovery()
     */
        Log.d(TAG, "Stopping Bluetooth discovery.");
        btAdapter.cancelDiscovery();

//        showNotification("Tap to open OBD-Reader", "Starting OBD connection..", R.drawable.ic_launcher, true, true, false);

        try {
            startObdConnection();
        } catch (Exception e) {
            Log.e(
                    TAG,
                    "There was an error while establishing connection. -> "
                            + e.getMessage()
            );

            // in case of failure, stop this service.
            stopService();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for (Timer t : connectionMonitorWorker) {
            t.cancel();
        }
    }

    /**
     * Start and configure the connection to the OBD interface.
     * <p/>
     * See http://stackoverflow.com/questions/18657427/ioexception-read-failed-socket-might-closed-bluetooth-on-android-4-3/18786701#18786701
     *
     * @throws IOException
     */
    private void startObdConnection() throws IOException {
        Log.d(TAG, "Starting OBD connection..");
        for (Timer t : connectionMonitorWorker) {
            t.cancel();
        }
        if (connectionStatusListener == null) {
            Log.e(TAG, "connectionStatusListener is not set!");
            stopService();
            return;
        }
        connectionStatusListener.btConnectionStatusChange(ObdProgressListener.BT_STATUS_CONNECTING);
        try {
            // Instantiate a BluetoothSocket for the remote device and connect it.
            sock = dev.createRfcommSocketToServiceRecord(MY_UUID);
            sock.connect();
        } catch (Exception e1) {
            connectionStatusListener.btConnectionStatusChange(ObdProgressListener.BT_STATUS_ERROR);
            Log.e(TAG, "There was an error while establishing Bluetooth connection. Falling back..", e1);
            Class<?> clazz = sock.getRemoteDevice().getClass();
            Class<?>[] paramTypes = new Class<?>[]{Integer.TYPE};
            try {
                Method m = clazz.getMethod("createRfcommSocket", paramTypes);
                Object[] params = new Object[]{Integer.valueOf(1)};
                sockFallback = (BluetoothSocket) m.invoke(sock.getRemoteDevice(), params);
                sockFallback.connect();
                sock = sockFallback;
            } catch (Exception e2) {
                connectionStatusListener.btConnectionStatusChange(ObdProgressListener.BT_STATUS_ERROR);
                Log.e(TAG, "Couldn't fallback while establishing Bluetooth connection. Stopping app..", e2);
                stopService();
                return;
            }
        }
        // Let's configure the connection.
        Log.d(TAG, "Queing jobs for connection configuration.." + sock);
        InputStream in = sock.getInputStream();
        Log.d(TAG, "InputStream:" + in);

        OutputStream out = sock.getOutputStream();
        Log.d(TAG, "OutputStream :" + out);
        // initial delay
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        connectionStatusListener.btConnectionStatusChange(ObdProgressListener.BT_STATUS_CONNECTED);

        String initString = config.getInitCommands();
        queueJobs(ObdHelper.buildJobList(initString, connectionStatusListener), false);


        queueCounter = 0L;
        Log.d(TAG, "Initialization jobs queued.");

        isRunning = true;
        createMonitor();

    }

    protected void createMonitor() {
        Timer t = new Timer(true);

        ObdTimerTask rpmTask = new ObdTimerTask("010C", connectionStatusListener, this, false);
        t.scheduleAtFixedRate(rpmTask, 2000, 10000);

        connectionMonitorWorker.add(t);

    }

    /**
     * Runs the queue until the service is stopped
     */
    synchronized protected void executeQueue() {
        synchronized (isQueueRunning) {
            isQueueRunning = true;
            while (!jobsQueue.isEmpty()) {
                ObdCommandJob job = null;
                try {
                    job = jobsQueue.take();

                    // log job
//                    Log.d(TAG, "Taking job[" + job.getCommand().getRawCommand() + "] from queue..");

                    if (job.getState().equals(ObdCommandJobState.NEW)) {
                        job.setState(ObdCommandJobState.RUNNING);
                        int cnt = sock.getInputStream().available();
                        // skip read old data
                        while (sock.getInputStream().available() != 0) {
                            sock.getInputStream().read();
                        }
                        job.getCommand().run(sock.getInputStream(), sock.getOutputStream());

                        if (job.getListener() != null) {
                            job.getListener().stateUpdate(job);
                        }

                        while (job.getCommand().isMoreData()) {
                            // post current and read more
                            job.getCommand().runMT(sock.getInputStream());
                            if (job.getListener() != null) {
                                job.getListener().stateUpdateMT(job.getCommand().getResult());
                            }

                            // stop if there is next command
                            if (!jobsQueue.isEmpty()) {
                                break;
                            }
                        }

                    } else
                        // log not new job
                        Log.e(TAG,
                                "Job state was not new, so it shouldn't be in queue. BUG ALERT!");
                } catch (Exception e) {
                    job.setState(ObdCommandJobState.EXECUTION_ERROR);
                    Log.e(TAG, "Failed to run command. -> " + e.getMessage());
                    if (job != null && job.getListener() != null) {
                        job.getListener().stateUpdate(job);
                    }
                }

            }
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // will run next time a job is queued
            isQueueRunning = false;
        }
    }

    /**
     * Stop OBD connection and queue processing.
     */
    public void stopService() {
        Log.d(TAG, "Stopping service..");
        Helper.cancelAllTasks(connectionMonitorWorker);
        if (notificationManager != null) {
            notificationManager.cancel(NOTIFICATION_ID);
        }
//        jobsQueue.removeAll(jobsQueue); // TODO is this safe?
        // safer way to make empty
        jobsQueue = new LinkedBlockingQueue<ObdCommandJob>();
        isRunning = false;

        if (sock != null)
            // close socket
            try {
                sock.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        // kill service

        stopSelf();



    }

    public boolean isRunning() {
        return isRunning;
    }

    public class ObdGatewayServiceBinder extends Binder {
        public ObdGatewayService getService() {
            return ObdGatewayService.this;
        }
    }

    public void reinitialize() {
        Helper.cancelAllTasks(connectionMonitorWorker);
        createMonitor();
    }
}