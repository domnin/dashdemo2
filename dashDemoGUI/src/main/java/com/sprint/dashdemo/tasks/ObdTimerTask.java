package com.sprint.dashdemo.tasks;

import java.util.TimerTask;

import pt.lighthouselabs.obd.reader.ObdProgressListener;
import pt.lighthouselabs.obd.reader.io.AbstractGatewayService;
import pt.lighthouselabs.obd.reader.util.ObdHelper;

/**
 * This class is used to be called on schedule to que one on many OBD commands
 */
public class ObdTimerTask extends TimerTask {
    private String commands;
    private ObdProgressListener listener;
    private AbstractGatewayService obdGatewayService;
    boolean canSkip;

    /**
     * @param commands - list of comma separated OBD commands
     * @param listener - listener class to call back
     * @param service  - OBD Gateway service
     */
    public ObdTimerTask(String commands, ObdProgressListener listener, AbstractGatewayService service, boolean canSkip) {
        this.commands = commands;
        this.listener = listener;
        obdGatewayService = service;
        this.canSkip = canSkip;
    }

    @Override
    public void run() {
        if (obdGatewayService != null) {
            obdGatewayService.queueJobs(ObdHelper.buildJobList(commands, listener), canSkip);
        }
    }
}
