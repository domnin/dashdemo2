package com.sprint.dashdemo.gui.util;

import android.util.Log;
import android.webkit.WebView;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.Random;

import pt.lighthouselabs.obd.reader.io.ObdCommandJob;
import pt.lighthouselabs.obd.reader.util.Logger;

/**
 * Created by dyd0912 on 2/4/2015.
 */
public class GaugesListener extends AbstractObdProgressListener {
    public GaugesListener(WebView webView) {
        super(webView);
    }

    @Override
    public void stateUpdate(ObdCommandJob job) {
        String res = job.getCommand().getTrimmedResult();
        Logger.d(res);
        try {
            if (res.startsWith("410C") && res.length() >= 8) {

                String data = res.substring(4);
                BigInteger bi = new BigInteger(data, 16);
                BigInteger mask = new BigInteger("FFFF", 16);
                bi = bi.and(mask).shiftRight(2);
                Log.d(TAG, "Calculated RPM:" + bi.toString());
                String rpm = bi.toString();
                int irpm = Integer.parseInt(rpm);

                float frpm = (float) (irpm / 1000.);
                frpm = emulate(frpm, 2f, 5f);
                UI.runJS(webView, "setRPM(" + frpm + ")");
//                UI.runJS(webView, "setRPM(4.5)");

            } else if (res.startsWith("410D") && res.length() >= 6) {

                String data = res.substring(4);
                BigInteger bi = new BigInteger(data, 16);
                BigInteger mask = new BigInteger("FF", 16);
                bi = bi.and(mask);
                Log.d(TAG, "Calculated Speed:" + bi.toString());
                String speed = bi.toString();
                int ispeed = Integer.parseInt(speed);
                float fspeed = ispeed * 0.621371192f;
                if (fspeed > 119) {
                    fspeed = emulate(0, 20f, 50f);
                }
                UI.runJS(webView, "setSpeed(" + fspeed + ")"); // km/h
//                UI.runJS(webView, "setSpeed(77)"); // km/h

            } else if (res.startsWith("412F") && res.length() >= 6) {

                String data = res.substring(4);
                BigInteger bi = new BigInteger(data, 16);
                BigInteger mask = new BigInteger("FF", 16);
                bi = bi.and(mask);
                Log.d(TAG, "Calculated Fuel:" + bi.toString());
                String fuel = bi.toString();
                int ifuel = Integer.parseInt(fuel);
                float ffuel = ifuel * 100f / 255f;
                UI.runJS(webView, "setFuel(" + (int) ffuel + ")"); // %%

            } else if (res.startsWith("4111") && res.length() >= 6) {

                String data = res.substring(4);
                BigInteger bi = new BigInteger(data, 16);
                BigInteger mask = new BigInteger("FF", 16);
                bi = bi.and(mask);
                Log.d(TAG, "Calculated Torque:" + bi.toString());
                String fuel = bi.toString();
                int ifuel = Integer.parseInt(fuel);
                float ffuel = ifuel * 100f / 255f;
                UI.runJS(webView, "setTorque(" + (int) ffuel + ")"); // %%

            } else if (res.startsWith("4105") && res.length() >= 6) {

                String data = res.substring(4);
                BigInteger bi = new BigInteger(data, 16);
                BigInteger mask = new BigInteger("FF", 16);
                bi = bi.and(mask);
                Log.d(TAG, "Calculated Temperature:" + bi.toString());
                String temp = bi.toString();
//                    fuel = "50";
                int iTemp = Integer.parseInt(temp) - 40;

//                float ftemp = (float)(iTemp* 1.8f + 32f); to fahrenheit

                float ftemp = iTemp / 2f; // from 1 to 128
                UI.runJS(webView, "setTemp(" + ftemp + ")"); //%%

            } else if (res.startsWith("415C") && res.length() >= 6) {

                String data = res.substring(4);
                BigInteger bi = new BigInteger(data, 16);
                BigInteger mask = new BigInteger("FF", 16);
                bi = bi.and(mask);
                Log.d(TAG, "Calculated Temperature:" + bi.toString());
                String temp = bi.toString();
                int iTemp = Integer.parseInt(temp) - 40;

//                float ftemp = (float)(iTemp* 1.8f + 32f); to fahrenheit

                float ftemp = iTemp / 2f; // from 1 to 128
                UI.runJS(webView, "setOilEngine(" + ftemp + ")"); //%%

            } else if (res.startsWith("4146") && res.length() >= 6) {

                String data = res.substring(4);
                BigInteger bi = new BigInteger(data, 16);
                BigInteger mask = new BigInteger("FF", 16);
                bi = bi.and(mask);
                Log.d(TAG, "Calculated Temperature:" + bi.toString());
                String temp = bi.toString();
//                    fuel = "50";
                int iTemp = Integer.parseInt(temp) - 40;

                float ftemp = (float) (iTemp * 1.8f + 32f); //to fahrenheit
                String val = new DecimalFormat("#.#").format(ftemp);
                UI.runJS(webView, "setOutsideTemp(" + val + ")"); //%%

            } else if (res.startsWith("410F") && res.length() >= 6) {

                String data = res.substring(4);
                BigInteger bi = new BigInteger(data, 16);
                BigInteger mask = new BigInteger("FF", 16);
                bi = bi.and(mask);
                Log.d(TAG, "Calculated Intake Temperature:" + bi.toString());
                String temp = bi.toString();
//                    fuel = "50";
                int iTemp = Integer.parseInt(temp) - 40;

                float ftemp = (float) (iTemp * 1.8f + 32f); //to fahrenheit
                String val = new DecimalFormat("#.#").format(ftemp);
                UI.runJS(webView, "setIntakeTemp(" + val + ")"); //%%

            } else if (res.startsWith("413C") && res.length() >= 6) {
                // Catalyst Temperature
                String data = res.substring(4);
                BigInteger bi = new BigInteger(data, 16);
                BigInteger mask = new BigInteger("FFFF", 16);
                bi = bi.and(mask);
                Log.d(TAG, "Calculated Catalyst Temperature:" + bi.toString());
                String temp = bi.toString();
//                    fuel = "50";
                int iTemp = Integer.parseInt(temp);

                float ftemp = ((iTemp / 10f - 40) * 1.8f + 32f); //to fahrenheit
                String val = new DecimalFormat("#.").format(ftemp);

                UI.runJS(webView, "setCatalystTemp(" + val + ")"); //%%

            } else if (res.startsWith("4104") && res.length() >= 6) {

                String data = res.substring(4);
                BigInteger bi = new BigInteger(data, 16);
                BigInteger mask = new BigInteger("FF", 16);
                bi = bi.and(mask);
                Log.d(TAG, "Calculated Temperature:" + bi.toString());
                String temp = bi.toString();
                int iLoad = Integer.parseInt(temp);
                float fLoad = iLoad * 100f / 255f; // %%
                UI.runJS(webView, "setOilEngine(" + fLoad + ")"); //%%

            } else if (res.startsWith("4142") && res.length() >= 8) {

                String data = res.substring(4);
                BigInteger bi = new BigInteger(data, 16);
                BigInteger mask = new BigInteger("FFFF", 16);
                bi = bi.and(mask); // v*1000
                Log.d(TAG, "Calculated Voltage:" + bi.toString());
                String volt = bi.toString();
                int ivolt = Integer.parseInt(volt);
                Log.d(TAG, "Voltage:" + ivolt / 1000f);
                float fvolt = ivolt / 1000f;
                String val = new DecimalFormat("#.#").format(fvolt);
                UI.runJS(webView, "setBatteryVoltage(" + val + ")");

            } else if (res.startsWith("4121") && res.length() >= 8) {

                String data = res.substring(4);
                BigInteger bi = new BigInteger(data, 16);
                BigInteger mask = new BigInteger("FFFF", 16);
                bi = bi.and(mask); // v*1000
                Log.d(TAG, "Calculated Distance:" + bi.toString());
                String dist = bi.toString();
                int idist = Integer.parseInt(dist);
                Log.d(TAG, "Distance:" + idist * 0.621371f);
                float fdist = idist * 0.621371f;
                String val = new DecimalFormat("#.#").format(fdist*10f);
                UI.runJS(webView, "setOdometer(" + val + ")");

            }
        } catch (Throwable e) {
            Logger.e("error in GaugesListener " + e.getMessage());
        }

    }

    private float emulate(float val, float min, float max) {
        if (val == 0.0) {
            float rnd = new Random().nextFloat();
            return rnd * (max - min) + min;

        } else return val;
    }
}
