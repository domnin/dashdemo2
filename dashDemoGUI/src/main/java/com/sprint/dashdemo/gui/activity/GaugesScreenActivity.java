package com.sprint.dashdemo.gui.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.google.inject.Inject;
import com.sprint.dashdemo.gui.R;
import com.sprint.dashdemo.gui.SideNavigationWindow;
import com.sprint.dashdemo.gui.util.ConnectionStatusListener;
import com.sprint.dashdemo.gui.util.GaugesListener;
import com.sprint.dashdemo.gui.util.Helper;
import com.sprint.dashdemo.gui.util.UI;
import com.sprint.dashdemo.service.ObdGatewayService;
import com.sprint.dashdemo.tasks.ObdTimerTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Timer;

import pt.lighthouselabs.obd.reader.ObdProgressListener;
import pt.lighthouselabs.obd.reader.io.AbstractGatewayService;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import wei.mark.standout.StandOutWindow;

public class GaugesScreenActivity extends RoboActivity {
    public static final String TAG = GaugesScreenActivity.class.getName();
    private static final int TABLE_ROW_MARGIN = 7;
    private static ArrayList<String> scrollData = new ArrayList<String>();
    private static String formulaString;
    @InjectView(R.id.web_content_gauges_fs)
    private WebView gaugesView;
    final List<Timer> workers = new ArrayList<Timer>();
    ObdProgressListener connectionStatusListener;

    @Inject
    private PowerManager powerManager;


    @Inject
    private SharedPreferences prefs;
    private boolean isServiceBound;
    private SideNavigationWindow service;
    private ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            Log.d(TAG, className.toString() + " service is bound");
            isServiceBound = true;
            service = ((SideNavigationWindow.ServiceBinder) binder).getService();

//            service.setConnectionListener(connectionStatusListener);
//            Log.d(TAG, "Starting the live data");
//            service.startService();
            reinitialize();

        }

        // This method is *only* called when the connection to the service is lost unexpectedly
        // and *not* when the client unbinds (http://developer.android.com/guide/components/bound-services.html)
        // So the isServiceBound attribute should also be set to false when we unbind from the service.
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.d(TAG, className.toString() + " service is unbound");
            isServiceBound = false;
        }
    };


    private PowerManager.WakeLock wakeLock = null;
    private boolean isLoop = false;

    // to handle java script calls from html
    class JS {
        @JavascriptInterface
        public void onPageLoaded() {

            new Handler(getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    startLiveData();
                }
            }, 2000);
        }


        @JavascriptInterface
        public void settings() {
            Log.d(TAG, "settings....");
            SideNavigationWindow.setKeepHidden(true);
            showConfig();
        }

        @JavascriptInterface
        public void set(boolean b) {
            Log.d(TAG, "set" + b);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.gauges_layout);
        JS js = new JS();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getActionBar().hide();
        gaugesView.getSettings().setJavaScriptEnabled(true);
        gaugesView.addJavascriptInterface(js, "JS");
        gaugesView.loadUrl("file:///android_asset/gauges.html");
        gaugesView.loadUrl("javascript:setFuel(50)");
        connectionStatusListener = new ConnectionStatusListener(gaugesView) {

            @Override
            public void createTasks() {
                reinitialize();
            }
        };

    }

    private void showConfig() {
        startActivity(new Intent(this, ConfigActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_test_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == pt.lighthouselabs.obd.reader.R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    protected void doBindService() {
        if (!isServiceBound) {
            UI.runJS(gaugesView, "carStatusConnecting()");

            Intent serviceIntent = new Intent(this, SideNavigationWindow.class);
            bindService(serviceIntent, serviceConn, Context.BIND_AUTO_CREATE);

        }
    }

    protected void doUnbindService() {
        if (isServiceBound) {
            Log.d(TAG, "Unbinding OBD service..");
            unbindService(serviceConn);
            isServiceBound = false;
        }
    }


    private void startLiveData() {
        Log.d(TAG, "Starting live data..");

        doBindService();
        // screen won't turn off until wakeLock.release()
        wakeLock.acquire();
    }


    private void stopLiveData() {
        Log.d(TAG, "Stopping live data..");

        doUnbindService();

        releaseWakeLockIfHeld();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "Pausing..");
        Helper.cancelAllTasks(workers);
        stopLiveData();
        if (!SideNavigationWindow.isKeepHidden()) {
            StandOutWindow
                    .show(this, SideNavigationWindow.class, StandOutWindow.DEFAULT_ID);
        }
        SideNavigationWindow.setKeepHidden(false);
//        releaseWakeLockIfHeld();
    }

    /**
     * If lock is held, release. Lock will be held when the service is running.
     */
    private void releaseWakeLockIfHeld() {
        if (wakeLock.isHeld())
            wakeLock.release();
    }

    protected void onResume() {
        super.onResume();
        StandOutWindow.closeAll(this, SideNavigationWindow.class);
        Log.d(TAG, "Resuming..");

        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,
                "ObdReader");

    }

    private void reinitialize() {
        //clean first
        Helper.cancelAllTasks(workers);

        // RPM task
        ObdProgressListener dashListener = new GaugesListener(gaugesView);
        Timer t = new Timer(true);

        ObdTimerTask rpmTask = new ObdTimerTask("010C,010D,0104,0111", dashListener, service.getObdService(), true);
        t.scheduleAtFixedRate(rpmTask, 200, 1000);

        ObdTimerTask oneMinuteTask = new ObdTimerTask("012F,0105,0142,0146,010F,013C,0121", dashListener, service.getObdService(), false);
        t.scheduleAtFixedRate(oneMinuteTask, 2000, 60000);
        workers.add(t);

    }
}



