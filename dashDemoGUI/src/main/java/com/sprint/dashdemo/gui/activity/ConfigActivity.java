package com.sprint.dashdemo.gui.activity;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.widget.Toast;

import com.sprint.dashdemo.gui.R;
import com.sprint.dashdemo.gui.SideNavigationWindow;
import com.sprint.obd.Constants;

import java.util.ArrayList;
import java.util.Set;

import pt.lighthouselabs.obd.commands.ObdCommand;
import pt.lighthouselabs.obd.reader.config.ObdConfig;
import wei.mark.standout.StandOutWindow;

/**
 * Configuration pt.lighthouselabs.obd.reader.activity.
 */
public class ConfigActivity extends PreferenceActivity implements
    OnPreferenceChangeListener {

    /**
   * @param prefs
   * @return
   */
  public static int getUpdatePeriod(SharedPreferences prefs) {
    String periodString = prefs
        .getString(Constants.UPDATE_PERIOD_KEY, "4"); // 4 as in seconds
    int period = 4000; // by default 4000ms

    try {
      period = Integer.parseInt(periodString) * 1000;
    } catch (Exception e) {
    }

    if (period <= 0) {
      period = 250;
    }

    return period;
  }

  /**
   * @param prefs
   * @return
   */
  public static double getVolumetricEfficieny(SharedPreferences prefs) {
    String veString = prefs.getString(Constants.VOLUMETRIC_EFFICIENCY_KEY,
        ".85");
    double ve = 0.85;
    try {
      ve = Double.parseDouble(veString);
    } catch (Exception e) {
    }
    return ve;
  }

  /**
   * @param prefs
   * @return
   */
  public static double getEngineDisplacement(SharedPreferences prefs) {
    String edString = prefs.getString(Constants.ENGINE_DISPLACEMENT_KEY,
        "1.6");
    double ed = 1.6;
    try {
      ed = Double.parseDouble(edString);
    } catch (Exception e) {
    }
    return ed;
  }


  /**
   * @param prefs
   * @return
   */
  public static double getMaxFuelEconomy(SharedPreferences prefs) {
    String maxStr = prefs.getString(Constants.MAX_FUEL_ECON_KEY, "70");
    double max = 70;
    try {
      max = Double.parseDouble(maxStr);
    } catch (Exception e) {
    }
    return max;
  }

  /**
   * @param prefs
   * @return
   */
  public static String[] getReaderConfigCommands(SharedPreferences prefs) {
    String cmdsStr = prefs.getString(Constants.CONFIG_READER_KEY, "atsp0\natz");
    String[] cmds = cmdsStr.split("\n");
    return cmds;
  }

  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    /*
     * Read preferences resources available at res/xml/preferences.xml
     */
    addPreferencesFromResource(R.xml.preferences);

    ArrayList<CharSequence> pairedDeviceStrings = new ArrayList<CharSequence>();
    ArrayList<CharSequence> vals = new ArrayList<CharSequence>();
    ListPreference listBtDevices = (ListPreference) getPreferenceScreen()
        .findPreference(Constants.BLUETOOTH_LIST_KEY);


    /*
     * Available OBD commands
     *
     * TODO This should be read from preferences database
     */
    ArrayList<ObdCommand> cmds = ObdConfig.getCommands();
    PreferenceScreen cmdScr = (PreferenceScreen) getPreferenceScreen()
        .findPreference(Constants.COMMANDS_SCREEN_KEY);


    /*
     * Let's use this device Bluetooth adapter to select which paired OBD-II
     * compliant device we'll use.
     */
    final BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();
    if (mBtAdapter == null) {
      listBtDevices
          .setEntries(pairedDeviceStrings.toArray(new CharSequence[0]));
      listBtDevices.setEntryValues(vals.toArray(new CharSequence[0]));

      // we shouldn't get here, still warn user
      Toast.makeText(this, "This device does not support Bluetooth.",
          Toast.LENGTH_LONG);

      return;
    }

    /*
     * Listen for preferences click.
     *
     * TODO there are so many repeated validations :-/
     */
    final Activity thisActivity = this;
    listBtDevices.setEntries(new CharSequence[1]);
    listBtDevices.setEntryValues(new CharSequence[1]);
    listBtDevices.setOnPreferenceClickListener(new OnPreferenceClickListener() {
      public boolean onPreferenceClick(Preference preference) {
        // see what I mean in the previous comment?
        if (mBtAdapter == null || !mBtAdapter.isEnabled()) {
          Toast.makeText(thisActivity,
              "This device does not support Bluetooth or it is disabled.",
              Toast.LENGTH_LONG);
          return false;
        }
        return true;
      }
    });

    /*
     * Get paired devices and populate preference list.
     */
    Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();
    if (pairedDevices.size() > 0) {
      for (BluetoothDevice device : pairedDevices) {
        pairedDeviceStrings.add(device.getName() + "\n" + device.getAddress());
        vals.add(device.getAddress());
      }
    }
    listBtDevices.setEntries(pairedDeviceStrings.toArray(new CharSequence[0]));
    listBtDevices.setEntryValues(vals.toArray(new CharSequence[0]));
  }

  /**
   * OnPreferenceChangeListener method that will validate a preferencen new
   * value when it's changed.
   *
   * @param preference the changed preference
   * @param newValue   the value to be validated and set if valid
   */
  public boolean onPreferenceChange(Preference preference, Object newValue) {
    if (Constants.UPDATE_PERIOD_KEY.equals(preference.getKey())
        || Constants.VOLUMETRIC_EFFICIENCY_KEY.equals(preference.getKey())
        || Constants.ENGINE_DISPLACEMENT_KEY.equals(preference.getKey())
        || Constants.UPDATE_PERIOD_KEY.equals(preference.getKey())
        || Constants.MAX_FUEL_ECON_KEY.equals(preference.getKey())) {
      try {
        Double.parseDouble(newValue.toString());
        return true;
      } catch (Exception e) {
        Toast.makeText(this,
            "Couldn't parse '" + newValue.toString() + "' as a number.",
            Toast.LENGTH_LONG).show();
      }
    }
    return false;
  }
    @Override
    protected void onPause() {
        super.onPause();
        if (SideNavigationWindow.isKeepHidden()) {
            StandOutWindow
                    .show(this, SideNavigationWindow.class, StandOutWindow.DEFAULT_ID);
        }
        SideNavigationWindow.setKeepHidden(false);
//        releaseWakeLockIfHeld();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SideNavigationWindow.setKeepHidden(true);
    }

}