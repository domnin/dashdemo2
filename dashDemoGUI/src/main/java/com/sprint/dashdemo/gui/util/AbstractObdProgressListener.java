package com.sprint.dashdemo.gui.util;

import android.util.Log;
import android.webkit.WebView;

import pt.lighthouselabs.obd.reader.ObdProgressListener;
import pt.lighthouselabs.obd.reader.io.ObdCommandJob;

/**
 * Created by dyd0912 on 2/2/2015.
 */
public abstract class AbstractObdProgressListener implements ObdProgressListener {
    static final String TAG = "ObdProgressListener";
    WebView webView;

    public AbstractObdProgressListener(WebView webView) {
        if (webView == null) {
            throw new RuntimeException("AbstractObdProgressListener: WebView is not set!");
        }
        this.webView = webView;
    }

    @Override
    public void stateUpdate(ObdCommandJob job) {
        Log.d(TAG, "stateUpdate");
        switch (job.getState()) {
            case EXECUTION_ERROR:
                UI.runJS(webView, "carStatusError()");
                UI.runJS(webView, "btStatusError()");
                break;
            case FINISHED:
                UI.runJS(webView, "carStatusConnected()");
                UI.runJS(webView, "btStatusConnected()");
                break;

            default:
        }

    }

    @Override
    public void stateUpdateMT(String rawResult) {

        Log.d(TAG, "stateUpdateMT " + rawResult);

    }

    @Override
    public void addTableRow(String key, String val) {
        Log.d(TAG, "addTableRow " + key + ":" + val);
    }

    @Override
    public void btConnectionStatusChange(int status) {
        switch (status) {
            case BT_STATUS_ERROR:
                UI.runJS(webView, "carStatusError()");
                UI.runJS(webView, "btStatusError()");
                break;
            case BT_STATUS_CONNECTING:
                UI.runJS(webView, "carStatusConnecting()");
                UI.runJS(webView, "btStatusConnecting()");
                break;

            case BT_STATUS_CONNECTED:
                UI.runJS(webView, "btStatusConnected()");
                break;
            case BT_STATUS_DISCONNECTED:
                UI.runJS(webView, "carStatusOff()");
                UI.runJS(webView, "btStatusOff()");
                break;
        }
    }

    @Override
    public void carConnectionStatusChange(int status) {
        switch (status) {
            case CAR_STATUS_CONNECTED:
                UI.runJS(webView, "carStatusConnected()");
                break;
            case CAR_STATUS_CONNECTING:
                UI.runJS(webView, "carStatusConnecting()");
                break;
            case CAR_STATUS_DISCONNECTED:
                UI.runJS(webView, "carStatusOff()");
                break;
            case CAR_STATUS_ERROR:
                UI.runJS(webView, "carStatusError()");
                break;
        }

    }

}
