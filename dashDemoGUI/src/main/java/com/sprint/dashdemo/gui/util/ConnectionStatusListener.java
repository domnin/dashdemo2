package com.sprint.dashdemo.gui.util;

import android.util.Log;
import android.webkit.WebView;

import com.sprint.dashdemo.gui.activity.CarDoctorActivity;
import com.sprint.dashdemo.service.ObdGatewayService;

import java.math.BigInteger;

import pt.lighthouselabs.obd.reader.ObdProgressListener;
import pt.lighthouselabs.obd.reader.io.ObdCommandJob;

/**
 * Updates connection status based on responce
 * Created by dyd0912 on 2/5/2015.
 */
public class ConnectionStatusListener extends AbstractObdProgressListener implements Scheduler {
    public static final String TAG = ConnectionStatusListener.class.getName();

    protected final static char[] dtcLetters = {'P', 'C', 'B', 'U'};
    protected final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    private StringBuffer codes = null;
    private int howManyTroubleCodes = 0;

    public ConnectionStatusListener(WebView webView) {
        super(webView);
    }

    @Override
    public void stateUpdate(ObdCommandJob job) {
        super.stateUpdate(job);
        if (job.getCommand().getRawCommand() != null) {
            String cmd = job.getCommand().getRawCommand().replace(" ", "");
            if (job.getCommand().getResult() != null) {
                if (cmd.toUpperCase().startsWith("ATSP")) {
                    if (job.getCommand().getResult().toUpperCase().contains("OK")) {
                        UI.runJS(webView, "carStatusConnected()");
                        ObdGatewayService.isConnected=true;
                        createTasks();
                    }
                } else if (cmd.toUpperCase().startsWith("01")) {
                    if (job.getCommand().getResult().toUpperCase().startsWith("41")) {
                        UI.runJS(webView, "carStatusConnected()");
                        UI.runJS(webView, "btStatusConnected()");
                    }
                } else if (cmd.toUpperCase().startsWith("03")) {
//                cmd = job.getCommand().getTrimmedResult();
                    String data = job.getCommand().getResult();
                    Log.d(TAG, "Trouble Codes:" + data);
                    if (data != null) {
                        performCalculations(data);
                        if (codes != null) {
                            CarDoctorActivity.setErrorCodes(codes.toString());
                            if (CarDoctorActivity.isShown()) {
                                CarDoctorActivity.getActivity().refreshErrors();
                            }
                        }
                    }
                    Log.d(TAG, "Trouble Codes:" + howManyTroubleCodes + "\n" + codes.toString());
                }
            }
        }
    }

    /**
     * Callback after establishing car connection        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
     Display display = windowManager.getDefaultDisplay();

     */
    @Override
    public void createTasks() {

    }

    @Override
    public void btConnectionStatusChange(int status) {
        switch (status) {
            case ObdProgressListener.BT_STATUS_CONNECTED:
                UI.runJS(webView, "btStatusConnected()");
                break;
            case ObdProgressListener.BT_STATUS_DISCONNECTED:
                UI.runJS(webView, "btStatusOff()");
                break;
            case ObdProgressListener.BT_STATUS_ERROR:
                UI.runJS(webView, "btStatusError()");
                break;
            case ObdProgressListener.BT_STATUS_CONNECTING:
                UI.runJS(webView, "btStatusConnecting()");
                break;
        }
    }

    protected void performCalculations(String data) {

        // remove line separators
        String wd = data.replaceAll("[\r\n ]", "");
        boolean isCAN = false;
        if (data.contains(":")) {
            // remove "0:" "1:"....
            for (Integer i = 0; i < 10; i++) {
                wd = wd.replace(i + ":", "");
            }
            isCAN = true;
        }
        codes = new StringBuffer();
        howManyTroubleCodes = 0;
        int begin = wd.indexOf("43");

        {
            begin += 2;
            if (isCAN) {
                // read(skip) error codes count (for CAN protocol)
                BigInteger cnt = new BigInteger(wd.substring(begin, begin + 2), 16);
                int icnt = cnt.intValue();
                begin += 2;
            }

            for (int j = 0; j < 30; j++) {
                String dtc = "";

                byte b1 = hexStringToByteArray(wd.charAt(begin));

                int ch1 = ((b1 & 0xC0) >> 6);
                int ch2 = ((b1 & 0x30) >> 4);

                dtc += dtcLetters[ch1];
                dtc += hexArray[ch2];

                begin++;

                dtc += wd.substring(begin, begin + 3);
                // read till end of data
                if (dtc.equals("P0000") || dtc.equals("B2AAA")) {
                    return;
                }

                codes.append(dtc);
                codes.append('\n');
                howManyTroubleCodes++;
                begin += 3;
            }
        }
    }

    private byte hexStringToByteArray(char s) {
        return (byte) ((Character.digit(s, 16) << 4));
    }

};




