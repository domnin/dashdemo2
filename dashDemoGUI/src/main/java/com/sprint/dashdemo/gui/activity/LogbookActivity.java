package com.sprint.dashdemo.gui.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.google.inject.Inject;
import com.sprint.dashdemo.gui.R;
import com.sprint.dashdemo.gui.SideNavigationWindow;

import java.util.ArrayList;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import wei.mark.standout.StandOutWindow;

public class LogbookActivity extends RoboActivity {
    public static final String TAG = LogbookActivity.class.getName();
    private static final int TABLE_ROW_MARGIN = 7;
    private static ArrayList<String> scrollData = new ArrayList<String>();
    private static String formulaString;

    @InjectView(R.id.web_content_gauges_fs)
    private WebView gaugesView;

    @Inject
    private PowerManager powerManager;


    @Inject
    private SharedPreferences prefs;
    private boolean isServiceBound;
    private SideNavigationWindow service;

    private PowerManager.WakeLock wakeLock = null;
    private boolean isLoop = false;

    // to handle java script calls from html
    class Settings {
        @JavascriptInterface
        public void save(String s) {
            Log.d(TAG, "Settings:" + s);
            // save in app
            // ....
        }

        @JavascriptInterface
        public void appSettings(){
            startActivity(new Intent(LogbookActivity.this, SettingsActivity.class));
        }
        @JavascriptInterface
        public void onPageLoaded() {

        }


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.gauges_layout);
        Settings settings = new Settings();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getActionBar().hide();
        gaugesView.getSettings().setJavaScriptEnabled(true);
        gaugesView.addJavascriptInterface(settings, "Settings");
        gaugesView.loadUrl("file:///android_asset/log_book.html");


    }

    private void showConfig() {
        startActivity(new Intent(this, ConfigActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_test_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == pt.lighthouselabs.obd.reader.R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "Pausing..");
        if (!SideNavigationWindow.isKeepHidden()) {
            StandOutWindow
                    .show(this, SideNavigationWindow.class, StandOutWindow.DEFAULT_ID);
        }
        SideNavigationWindow.setKeepHidden(false);
//        releaseWakeLockIfHeld();
    }

    /**
     * If lock is held, release. Lock will be held when the service is running.
     */
    private void releaseWakeLockIfHeld() {
        if (wakeLock.isHeld())
            wakeLock.release();
    }

    protected void onResume() {
        super.onResume();
        StandOutWindow.closeAll(this, SideNavigationWindow.class);
        Log.d(TAG, "Resuming..");

        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,
                "ObdReader");

    }

}



