package com.sprint.dashdemo.gui.util;

import android.os.Handler;
import android.os.Looper;
import android.webkit.WebView;

/**
 * UI helper
 * Created by dyd0912 on 2/2/2015.
 */
public class UI {
    /**
     * Execute javascript on main thread.
     *
     * @param webView
     * @param js
     */
    public static void runJS(final WebView webView, final String js) {
        if (webView != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    webView.loadUrl("javascript:" + js);
                }
            });
        }
    }
}
