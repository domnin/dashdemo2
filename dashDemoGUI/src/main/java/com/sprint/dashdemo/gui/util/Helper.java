package com.sprint.dashdemo.gui.util;

import java.util.List;
import java.util.Timer;

/**
 * Created by dyd0912 on 2/4/2015.
 */
public final class Helper {
    /**
     * Cancel all Timer tasks and clean array of workers.
     * @param workers
     */
    public static void cancelAllTasks(List<Timer> workers) {
        for (Timer t : workers) {
            t.cancel();
        }
        workers.clear();
    }
}
