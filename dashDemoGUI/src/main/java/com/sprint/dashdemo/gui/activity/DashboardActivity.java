package com.sprint.dashdemo.gui.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;

import com.google.inject.Inject;
import com.sprint.dashdemo.gui.R;
import com.sprint.dashdemo.gui.SideNavigationWindow;

import java.util.ArrayList;

import pt.lighthouselabs.obd.commands.ObdCommand;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import wei.mark.standout.StandOutWindow;

public class DashboardActivity extends RoboActivity {
    public static final String TAG = DashboardActivity.class.getName();
    private static final int TABLE_ROW_MARGIN = 7;
    private static ArrayList<String> scrollData = new ArrayList<String>();
    private static String formulaString;

    @InjectView(R.id.web_content_gauges_fs)
    private WebView dashboardView;

    private ObdCommand obdCommand;

    // to handle java script calls from html
    class JS {

        @JavascriptInterface
        public void launchApp(String id) {
            Log.d(TAG, "launchApp:" + id);
            Intent launchIntent = getPackageManager().getLaunchIntentForPackage(id);
            if (launchIntent == null) {
                Log.d(TAG, "launchApp - NOT INSTALLED:" + id);
                Toast.makeText(DashboardActivity.this, "Application " + id + " is not installed!", Toast.LENGTH_LONG);
            } else {
                startActivity(launchIntent);
            }
        }
        @JavascriptInterface
        public void launchCarDoctor(){
            startActivity(new Intent(DashboardActivity.this, CarDoctorActivity.class));
        }

        @JavascriptInterface
        public void settings() {
            Log.d(TAG, "settings....");
            SideNavigationWindow.setKeepHidden(true);
            showConfig();
        }

        @JavascriptInterface
        public void set(boolean b) {
            Log.d(TAG, "set" + b);
        }
    }

    @Inject
    private SharedPreferences prefs;

    private PowerManager.WakeLock wakeLock = null;
    private boolean isLoop = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.gauges_layout);
        JS js = new JS();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getActionBar().hide();
        dashboardView.getSettings().setJavaScriptEnabled(true);
        dashboardView.addJavascriptInterface(js, "JS");
        dashboardView.loadUrl("file:///android_asset/mediadashbrd.html");


    }

    private void showConfig() {

//        startActivity(new Intent(this, ConfigActivity.class));
        startActivity(new Intent(this, SettingsActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_test_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == pt.lighthouselabs.obd.reader.R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "Pausing..");
        if (!SideNavigationWindow.isKeepHidden()) {
            StandOutWindow
                    .show(this, SideNavigationWindow.class, StandOutWindow.DEFAULT_ID);
        }
        SideNavigationWindow.setKeepHidden(false);
//        releaseWakeLockIfHeld();
    }

    /**
     * If lock is held, release. Lock will be held when the service is running.
     */
    private void releaseWakeLockIfHeld() {
//        if (wakeLock.isHeld())
//            wakeLock.release();
    }

    protected void onResume() {
        super.onResume();
        StandOutWindow.closeAll(this, SideNavigationWindow.class);

        Log.d(TAG, "Resuming..");

//        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,
//                "ObdReader");

    }


}



