package com.sprint.dashdemo.gui.activity;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.sprint.dashdemo.gui.SideNavigationWindow;

import wei.mark.standout.StandOutWindow;

public class StandOutExampleActivity extends Activity {
    static final String TAG = "StandOutExampleActivity";
    static StandOutExampleActivity mainActivity;
    public Gauges gauges = new Gauges();

    public static StandOutExampleActivity getMainActivity() {
        return mainActivity;
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = this;
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        Log.d(TAG, "Screen:" + width + "x" + height);
        StandOutWindow.closeAll(this, SideNavigationWindow.class);
//        StandOutWindow.closeAll(this, SimpleWindow.class);
//		StandOutWindow.closeAll(this, MultiWindow.class);
//		StandOutWindow.closeAll(this, WidgetsWindow.class);

        // show a MultiWindow, SimpleWindow

        StandOutWindow
                .show(this, SideNavigationWindow.class, StandOutWindow.DEFAULT_ID);
//        StandOutWindow
//                .show(this, GaugesWindow.class, StandOutWindow.DEFAULT_ID);
//        StandOutWindow
//                .show(this, SimpleWindow.class, StandOutWindow.DEFAULT_ID);
//		StandOutWindow.show(this, MultiWindow.class, StandOutWindow.DEFAULT_ID);
//		StandOutWindow.show(this, WidgetsWindow.class,
//				StandOutWindow.DEFAULT_ID);

        // show a MostBasicWindow. It is commented out because it does not
        // support closing.

		/*
         * StandOutWindow.show(this, StandOutMostBasicWindow.class,
		 * StandOutWindow.DEFAULT_ID);
		 */

        finish();
    }

    class Gauges {
        public void setFullScreen() {
            Log.d(TAG, "Gauges.fullscreen");
        }
    }
}