package com.sprint.dashdemo.gui.activity;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.google.inject.Inject;
import com.sprint.dashdemo.google.AlertDialogManager;
import com.sprint.dashdemo.google.GPSTracker;
import com.sprint.dashdemo.google.GooglePlaces;
import com.sprint.dashdemo.google.Place;
import com.sprint.dashdemo.google.PlaceDetails;
import com.sprint.dashdemo.google.PlacesList;
import com.sprint.dashdemo.gui.R;
import com.sprint.dashdemo.gui.SideNavigationWindow;
import com.sprint.dashdemo.gui.util.CodeList;
import com.sprint.dashdemo.gui.util.ConnectionStatusListener;
import com.sprint.dashdemo.gui.util.EngineCode;
import com.sprint.dashdemo.gui.util.Helper;
import com.sprint.dashdemo.gui.util.UI;
import com.sprint.dashdemo.tasks.ObdTimerTask;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;

import pt.lighthouselabs.obd.reader.ObdProgressListener;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import wei.mark.standout.StandOutWindow;

public class CarDoctorActivity extends RoboActivity {
    public static final String TAG = CarDoctorActivity.class.getName();
    private static final int TABLE_ROW_MARGIN = 7;
    private static final List<String> errorCodes = new ArrayList<String>();
    // KEY Strings
    public static String KEY_REFERENCE = "reference"; // id of the place
    public static String KEY_NAME = "name"; // name of the place
    public static String KEY_VICINITY = "vicinity"; // Place area name
    private static ArrayList<String> scrollData = new ArrayList<String>();
    private static String formulaString;
    private static boolean isShown;
    private static CarDoctorActivity carDoctorActivity;
    final List<Timer> workers = new ArrayList<Timer>();
    // ListItems data
    ArrayList<HashMap<String, String>> placesListItems = new ArrayList<HashMap<String, String>>();
    GooglePlaces googlePlaces;
    PlacesList nearPlaces;
    ProgressDialog ringProgressDialog;
    ObdProgressListener connectionStatusListener;
    AlertDialogManager alert = new AlertDialogManager();

    // GPS Location
    GPSTracker gps;
    @InjectView(R.id.web_content_gauges_fs)
    private WebView carDoctorView;
    @Inject
    private PowerManager powerManager;
    @Inject
    private SharedPreferences prefs;
    private boolean isServiceBound;
    private SideNavigationWindow service;
    private ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            Log.d(TAG, className.toString() + " service is bound");
            isServiceBound = true;
            service = ((SideNavigationWindow.ServiceBinder) binder).getService();

//            service.setConnectionListener(connectionStatusListener);
//            Log.d(TAG, "Starting the live data");
//            service.startService();
            reinitialize();

        }

        // This method is *only* called when the connection to the service is lost unexpectedly
        // and *not* when the client unbinds (http://developer.android.com/guide/components/bound-services.html)
        // So the isServiceBound attribute should also be set to false when we unbind from the service.
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.d(TAG, className.toString() + " service is unbound");
            isServiceBound = false;
        }
    };
    private String json = "{" +
            "\"problem\": [" +
            "{" +
            "   \"name\": \"P0122\"," +
            "\"description\": \"Throttle Position Sensor/Switch A Circuit Low Input\"," +
            "\"severity\": \"1\"," +
            "\"symptoms\": [\"Engine Light ON (or Service Engine Soon Warning Light)\"]," +
            "\"details\": \"details..\"," +
            "\"difficulty\": \"3\"," +
            "\"dtc\": \"P0000\"" +
            "}" +
            "]" +
            "}";
    private PowerManager.WakeLock wakeLock = null;
    private boolean isLoop = false;

    public static void setErrorCodes(String errors) {
        if (errors != null) {
            errorCodes.clear();
            errorCodes.addAll(Arrays.asList(errors.split("\n")));
        }
    }

    public static boolean isShown() {
        return isShown;
    }

    public static CarDoctorActivity getActivity() {
        return carDoctorActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        carDoctorActivity = this;
        setContentView(R.layout.gauges_layout);
        JS JS = new JS();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getActionBar().hide();
        carDoctorView.getSettings().setJavaScriptEnabled(true);
        carDoctorView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        carDoctorView.addJavascriptInterface(JS, "JS");
        carDoctorView.loadUrl("file:///android_asset/cardoctor.html");

        connectionStatusListener = new ConnectionStatusListener(carDoctorView) {

            @Override
            public void createTasks() {
                reinitialize();
            }
        };

        // creating GPS Class object
        gps = new GPSTracker(this);


        // check if GPS location can get
        if (gps.canGetLocation()) {
            Log.d("Your Location", "latitude:" + gps.getLatitude() + ", longitude: " + gps.getLongitude());
        } else {
            // Can't get user's current location
            alert.showAlertDialog(CarDoctorActivity.this, "GPS Status",
                    "Couldn't get location information. Please enable GPS",
                    false);
            // stop executing code by return
            //return;
        }
        new LoadPlaces().execute();

    }

    private void showConfig() {
        startActivity(new Intent(this, ConfigActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_test_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == pt.lighthouselabs.obd.reader.R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isShown = false;
        Log.d(TAG, "Pausing..");
        Helper.cancelAllTasks(workers);
        stopLiveData();
        if (!SideNavigationWindow.isKeepHidden()) {
            StandOutWindow
                    .show(this, SideNavigationWindow.class, StandOutWindow.DEFAULT_ID);
        }
        SideNavigationWindow.setKeepHidden(false);
//        releaseWakeLockIfHeld();
    }

    /**
     * If lock is held, release. Lock will be held when the service is running.
     */
    private void releaseWakeLockIfHeld() {
        if (wakeLock.isHeld())
            wakeLock.release();
    }

    protected void onResume() {
        super.onResume();
        isShown = true;
        StandOutWindow.closeAll(this, SideNavigationWindow.class);
        Log.d(TAG, "Resuming..");

        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,
                "ObdReader");

    }

    private void reinitialize() {
        //clean first
        Helper.cancelAllTasks(workers);

        // RPM task
        ObdProgressListener dashListener = new ConnectionStatusListener(carDoctorView);
        Timer t = new Timer(true);

        ObdTimerTask rpmTask = new ObdTimerTask("03,03", dashListener, service.getObdService(), true);
        t.scheduleAtFixedRate(rpmTask, 200, 10000000);

        workers.add(t);

    }

    protected void doBindService() {
        if (!isServiceBound) {
            UI.runJS(carDoctorView, "carStatusConnecting()");

            Intent serviceIntent = new Intent(this, SideNavigationWindow.class);
            bindService(serviceIntent, serviceConn, Context.BIND_AUTO_CREATE);

        }
    }

    protected void doUnbindService() {
        if (isServiceBound) {
            Log.d(TAG, "Unbinding OBD service..");
            unbindService(serviceConn);
            isServiceBound = false;
        }
    }

    private void startLiveData() {
        Log.d(TAG, "Starting live data..");

        doBindService();
        // screen won't turn off until wakeLock.release()
        wakeLock.acquire();
    }

    private void stopLiveData() {
        Log.d(TAG, "Stopping live data..");

        doUnbindService();

        releaseWakeLockIfHeld();
    }

    public void refreshErrors() {
        buildJsonFromErrorCodes();

        String script = "loadProblems('" + json + "')";
        UI.runJS(carDoctorView, script);
        ringProgressDialog.dismiss();
    }

    void buildJsonFromErrorCodes() {
        StringBuilder sb = new StringBuilder();
        AssetManager am = this.getAssets();
        try {
            InputStream in = am.open("error_codes.json");
            CodeList codeList = new ObjectMapper().readValue(in, CodeList.class);
            HashMap<String, EngineCode> db = new HashMap();

            for (EngineCode ec : codeList.getProblem()) {
                db.put(ec.getDtc(), ec);
            }

            sb.append("{\"problem\": [");
            EngineCode[] errors = new EngineCode[errorCodes.size()];
            int i = 0;
            for (String err : errorCodes) {
                errors[i++] = db.get(err);
            }
//            EngineCode[] errors = new EngineCode[1];
//            errors[0]= db.get("P2127");
//            errors[1]= db.get("P2122");
            CodeList cl = new CodeList();
            cl.setProblem(errors);
            json = new ObjectMapper().writeValueAsString(cl);

        } catch (Exception e) {
            Log.e(TAG, "error parsing codes:", e);
        }
    }

    private void displayPlaces() {
        try {
            String js = new ObjectMapper().writeValueAsString(nearPlaces);
            String script = "loadPlaces('" + js + "')";
            UI.runJS(carDoctorView, script);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // to handle java script calls from html
    class JS {
        @JavascriptInterface
        public void save(String s) {
            Log.d(TAG, "Settings:" + s);
            // save in app
            // ....
        }

        @JavascriptInterface
        public void onDtcClick(String code) {
            Log.d(TAG, "onDtcClick: " + code);
            displayPlaces();
        }

        @JavascriptInterface
        public void logBook() {
            startActivity(new Intent(CarDoctorActivity.this, LogbookActivity.class));
        }

        @JavascriptInterface
        public void onPageLoaded() {
            String script = "loadProblems('" + json + "')";
            UI.runJS(carDoctorView, script);
            ringProgressDialog = ProgressDialog.show(CarDoctorActivity.this, "Please wait ...", "Reading error codes...", true);
            new Handler(getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    startLiveData();
                }
            }, 2000);

        }


    }

    /**
     * Background Async Task to Load Google places
     */
    class LoadPlaces extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * getting Places JSON
         */
        protected String doInBackground(String... args) {
            // creating Places class object
            googlePlaces = new GooglePlaces();

            try {
                // Separeate your place types by PIPE symbol "|"
                // If you want all types places make it as null
                // Check list of types supported by google
                //
                String types = "car_repair"; // Listing places only cafes, restaurants

                // Radius in meters - increase this value if you don't find any places
                double radius = 1000; // 1000 meters

                // get nearest places
                nearPlaces = googlePlaces.search(gps.getLatitude(),
                        gps.getLongitude(), radius, types);
                List<Place> detailedResults = new ArrayList<Place>();
                if (nearPlaces.results != null) {
                    for (Place p : nearPlaces.results) {
                        PlaceDetails dp = googlePlaces.getPlaceDetails(p.reference);
                        detailedResults.add(dp.result);
                    }
                    nearPlaces.results = detailedResults;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * and show the data in UI
         * Always use runOnUiThread(new Runnable()) to update UI from background
         * thread, otherwise you will get error
         * *
         */
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed Places into LISTVIEW
                     * */
                    // Get json response status
                    String status = nearPlaces.status;

                    // Check for all possible status
                    if (status.equals("OK")) {
                        // Successfully got places details
                        if (nearPlaces.results != null) {
                            // loop through each place
                            for (Place p : nearPlaces.results) {
                                HashMap<String, String> map = new HashMap<String, String>();

                                // Place reference won't display in listview - it will be hidden
                                // Place reference is used to get "place full details"
                                map.put(KEY_REFERENCE, p.reference);

                                // Place name
                                map.put(KEY_NAME, p.name);
                                Log.d(TAG, "Place:" + p.name);


                                // adding HashMap to ArrayList
                                placesListItems.add(map);
                            }
                            // done
                            displayPlaces();
                            //ringProgressDialog.dismiss();

                        }
                    } else if (status.equals("ZERO_RESULTS")) {
                        // Zero results found
                        alert.showAlertDialog(CarDoctorActivity.this, "Near Places",
                                "Sorry no places found. Try to change the types of places",
                                false);
                    } else if (status.equals("UNKNOWN_ERROR")) {
                        alert.showAlertDialog(CarDoctorActivity.this, "Places Error",
                                "Sorry unknown error occured.",
                                false);
                    } else if (status.equals("OVER_QUERY_LIMIT")) {
                        alert.showAlertDialog(CarDoctorActivity.this, "Places Error",
                                "Sorry query limit to google places is reached",
                                false);
                    } else if (status.equals("REQUEST_DENIED")) {
                        alert.showAlertDialog(CarDoctorActivity.this, "Places Error",
                                "Sorry error occured. Request is denied",
                                false);
                    } else if (status.equals("INVALID_REQUEST")) {
                        alert.showAlertDialog(CarDoctorActivity.this, "Places Error",
                                "Sorry error occured. Invalid Request",
                                false);
                    } else {
                        alert.showAlertDialog(CarDoctorActivity.this, "Places Error",
                                "Sorry error occured.",
                                false);
                    }
                }
            });

        }

    }

}



