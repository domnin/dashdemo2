package com.sprint.dashdemo.gui;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;

import com.sprint.dashdemo.gui.activity.DashboardActivity;
import com.sprint.dashdemo.gui.activity.GaugesScreenActivity;
import com.sprint.dashdemo.gui.activity.StandOutExampleActivity;
import com.sprint.dashdemo.gui.util.ConnectionStatusListener;
import com.sprint.dashdemo.gui.util.GaugesListener;
import com.sprint.dashdemo.gui.util.Helper;
import com.sprint.dashdemo.gui.util.UI;
import com.sprint.dashdemo.service.ObdGatewayService;
import com.sprint.dashdemo.tasks.ObdTimerTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import pt.lighthouselabs.obd.reader.ObdProgressListener;
import pt.lighthouselabs.obd.reader.io.AbstractGatewayService;
import wei.mark.standout.StandOutWindow;
import wei.mark.standout.constants.StandOutFlags;
import wei.mark.standout.ui.Window;

/**
 * Transparent mini window which stays on top of other applications.
 */
public class SideNavigationWindow extends StandOutWindow {
    static final String TAG = "SideNavigationWindow";
    /**
     * Flag to check in onPause to keep window hidden when switching between activities.
     */
    static boolean keepHidden;
    final List<Timer> workers = new ArrayList<Timer>();
    private final IBinder binder = new ServiceBinder();
    ObdProgressListener connectionStatusListener;
    WebChromeClient wcc;
    int winId;
    private boolean isServiceBound;
    private AbstractGatewayService service;
    private ServiceConnection serviceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            Log.d(TAG, className.toString() + " service is bound");
            isServiceBound = true;
            service = ((AbstractGatewayService.AbstractGatewayServiceBinder) binder).getService();
            service.setContext(SideNavigationWindow.this);
            service.setConnectionListener(connectionStatusListener);
            Log.d(TAG, "=========Starting the live data");
            service.startService();

            Log.d(TAG, "=========Started");
        }

        // This method is *only* called when the connection to the service is lost unexpectedly
        // and *not* when the client unbinds (http://developer.android.com/guide/components/bound-services.html)
        // So the isServiceBound attribute should also be set to false when we unbind from the service.
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.d(TAG, className.toString() + " service is unbound");
            isServiceBound = false;
        }
    };
    private WebView gauge;

    public static boolean isKeepHidden() {
        return keepHidden;
    }

    public static void setKeepHidden(boolean keepHidden) {
        SideNavigationWindow.keepHidden = keepHidden;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public String getAppName() {
        return "DashDemo";
    }

    @Override
    public int getAppIcon() {
        return android.R.drawable.ic_menu_close_clear_cancel;
    }

    @Override
    public void createAndAttachView(int id, FrameLayout frame) {
        // create a new layout from body.xml
        winId = id;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.simple, frame, true);
        gauge = (WebView) frame.findViewById(R.id.web_content_rpm);
        gauge.setBackgroundColor(Color.TRANSPARENT);
        postCreate();

        final int winId = id;
        // make webview movable
        gauge.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // pass all touch events to the implementation
                boolean consumed = false;

                // handle move and bring to front
                consumed = onTouchHandleMove(winId, getWindow(winId), v, event)
                        || consumed;

                // alert implementation
                consumed = onTouchBody(winId, getWindow(winId), v, event)
                        || consumed;
// may check x,y to set consumed or not for a selected area
//                return consumed;
                return false; // to allow clicks in web view
            }
        });


        connectionStatusListener = new ConnectionStatusListener(gauge) {

            @Override
            public void createTasks() {
                reinitialize();
            }
        };
        if (service != null) {
            service.setConnectionListener(connectionStatusListener);
            ((ObdGatewayService) service).reinitialize();
        }
    }

    /*
        render html page in web view.
     */
    void postCreate() {
        gauge.getSettings().setJavaScriptEnabled(true);
        JS js = new JS();
        gauge.addJavascriptInterface(js, "JS");
        gauge.loadUrl("file:///android_asset/navigation.html");


        wcc = new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                Log.d(TAG, "onJsAlert:" + message);
                result.confirm();
                if ("fullscreen".equals(message)) {
                    Intent intent = new Intent(StandOutExampleActivity.getMainActivity(), GaugesScreenActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    StandOutExampleActivity.getMainActivity().startActivity(intent);
                    hide(winId);
                } else if ("dashboard".equals(message)) {
                    Intent intent = new Intent(StandOutExampleActivity.getMainActivity(), DashboardActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    StandOutExampleActivity.getMainActivity().startActivity(intent);
                    hide(winId);
                }


                return true;
            }

        };
        gauge.setWebChromeClient(wcc);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        doUnbindService();
    }

    // the window will be centered
    @Override
    public StandOutLayoutParams getParams(int id, Window window) {
        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        Log.d(TAG, "Screen:" + width + "x" + height);
        int h = 730 * height / 800;
        int w = 320 * h / 730;
        return new StandOutLayoutParams(id, w, h,
                StandOutLayoutParams.BOTTOM, StandOutLayoutParams.LEFT);
    }

    // move the window by dragging the view
    @Override
    public int getFlags(int id) {
        return super.getFlags(id) | StandOutFlags.FLAG_BODY_MOVE_ENABLE
                | StandOutFlags.FLAG_WINDOW_FOCUSABLE_DISABLE
                ;
    }

    @Override
    public String getPersistentNotificationMessage(int id) {
        return "Click to close the DashDemo";
    }

    @Override
    public Intent getPersistentNotificationIntent(int id) {
        return StandOutWindow.getCloseIntent(this, SideNavigationWindow.class, id);
    }

    protected void doBindService() {
        if (!isServiceBound) {
            UI.runJS(gauge, "carStatusConnecting()");

            Intent serviceIntent = new Intent(this, ObdGatewayService.class);
            bindService(serviceIntent, serviceConn, Context.BIND_AUTO_CREATE);

        }
    }

    protected void doUnbindService() {
        if (isServiceBound) {
            if (service.isRunning()) {
                service.stopService();
            }
            Log.d(TAG, "Unbinding OBD service..");
            unbindService(serviceConn);
            isServiceBound = false;
        }
    }

    @Override
    public boolean onClose(int id, Window window) {
        Log.d(TAG, "onClose=====");
        //cancel timers
        Helper.cancelAllTasks(workers);

//        doUnbindService();
        return super.onClose(id, window);
    }

    @Override
    public boolean onShow(int id, Window window) {
        Log.d(TAG, "onShow=====");

        return super.onShow(id, window);
    }

    public AbstractGatewayService getObdService() {
        return service;
    }

    private void reinitialize() {
        //clean first
        Helper.cancelAllTasks(workers);

        // RPM task
        ObdProgressListener dashListener = new GaugesListener(gauge);
        if (ObdGatewayService.isConnected) {
            Timer t = new Timer(true);

            ObdTimerTask rpmTask = new ObdTimerTask("010C,010D", dashListener, service, true);
            t.scheduleAtFixedRate(rpmTask, 300, 700);

            ObdTimerTask oneMinuteTask = new ObdTimerTask("012F,0105", dashListener, service, false);
            t.scheduleAtFixedRate(oneMinuteTask, 2000, 60000);
            workers.add(t);
        }
    }

    @Override
    public Intent getHiddenNotificationIntent(int id) {
        return super.getHiddenNotificationIntent(id);
    }

    class JS {
        @JavascriptInterface
        public void reconnect() {
            doUnbindService();
            doBindService();
        }

        /**
         * disconnect and stop all services
         */
        @JavascriptInterface
        public void disconnect() {
            Log.d(TAG, "disconnect");
            if (service != null && service.isRunning()) {
                Helper.cancelAllTasks(workers);
                System.exit(0); // hard exit .....
//                UI.runJS(gauge, "btStatusOff()");
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//
//                        doUnbindService();
//                        stopService(new Intent(SideNavigationWindow.this, ObdGatewayService.class));
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                stopService(new Intent(SideNavigationWindow.this, SideNavigationWindow.class));
//                            }
//                        }, 2000);
//                    }
//                }, 1000);
            }
        }

        @JavascriptInterface
        public void onPageLoaded() {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    doBindService();
                    if (isServiceBound && service != null) {
                        // set back to local listener
                        service.setConnectionListener(connectionStatusListener);
                        reinitialize();
                    }
                }
            }, 2000);
        }

        @JavascriptInterface
        public void set(boolean b) {
//            Log.d(TAG, "set " + b);
//            doBindService();

        }

        @JavascriptInterface
        public void dashboard() {

            Intent intent = new Intent(StandOutExampleActivity.getMainActivity(), DashboardActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }


        @JavascriptInterface
        public void fullscreen() {

            Intent intent = new Intent(StandOutExampleActivity.getMainActivity(), GaugesScreenActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
    }

    public class ServiceBinder extends Binder {
        public SideNavigationWindow getService() {
            return SideNavigationWindow.this;
        }
    }
}
