function loadProblems(json){
	
	//listing problems and their details and places to fix
	$('nav ol li').remove();
	$.each($.parseJSON(json)['problem'], function(i,problem) {
		$('nav ol').append('\
			<li>\
				<div><div onClick="JS.onDtcClick(\''+problem['dtc']+'\')">\
					<img src="images/cardoctor_severity'+problem['severity']+'.png"/>\
					<h4>'+problem['name']+'</h4>\
					'+problem['description']+'\
				</div></div>\
				<div>\
					<ul>\
						<li class="lidetails"><h5>Details</h5>'+problem['details']+'</li>\
						<li class="licauses"><h5>Possbile causes</h5><ul></ul></li>\
						<li class="lisymptoms"><h5>Possible symptoms</h5><ul></li></ul></li>\
					</ul>\
					<ul class="second">\
					</ul>\
				</div>\
			</li>\
		');
		$.each(problem['causes'], function(i,cause) {
			$('li.licauses:last ul').append('<li>'+cause+'</li>');			
		});		
		$.each(problem['symptoms'], function(i,symptom) {
			$('li.lisymptoms:last ul').append('<li>'+symptom+'</li>');			
		});		
	});	
	//accordion
	$('nav>ol>li').click(function(){ 
		if($(this).children('div+div').is(':visible')) { //if it's already open
			$('nav div+div').slideUp(); //just close it
		} else {
			$('nav div+div').slideUp(); //close others
			$(this).children('div+div').slideDown(); //open clicked
		}
	});
}

function loadPlaces(json){
	$.each($.parseJSON(json)['results'], function(i,place) {		
		$('section ul').append('\
			<li>\
				<div>\
					<p>'+place.vicinity+'</p>\
					<p>'+place.formatted_phone_number+'<a onclick="call(this)"></a></p>\
				</div>\
				<h5>'+place.name+'</h5>\
				<a href="'+place.website+'"></a><br/>\
				<strong>'+place.rating+'</strong>\
				<span>'+place.user_ratings_total+' reviews</span>\
			</li>\
		');
			if(place.website) {
				$('section li:last h5+a').text((place.website).split('/')[2].split('www.')[1]); //prints website name, without "http://www" neither codes after bars
			} else {
				$('section li:last h5+a').remove();
			}
			$('section li:last strong').css('min-width',17*place.rating);
			if(place.user_ratings_total==1)	$('section li:last span').html('1 review');
			if(!place.rating) $('section li:last strong, section li:last span').remove();
			if(!place.formatted_phone_number) $('section li:last p+p').remove();
	})
}

function call(link){	
	var phone = $(link).closest('p').text();
	alert('making a call to '+phone);
}