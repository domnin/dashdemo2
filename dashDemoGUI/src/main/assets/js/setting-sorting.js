$(function () {
    var helper;
    $("ul.customize-list").sortable({
			connectWith: 'ul.customize-list',        
			opacity: 0.6,
			revert: true,
			helper: function (e, item) {
				if(!item.hasClass('selected')) item.addClass('selected');
				var elements = $('.selected').not('.ui-sortable-placeholder').clone();
				var helper = $('<ul/>');
				item.siblings('.selected').addClass('hidden');
				return helper.append(elements);
			},
			receive: function (e, ui) {
				if($(this).attr('id')=='sortable2'){
					if($('#sortable2 li').length>3){
						$(ui.sender).sortable('cancel');
						alertMeassage('Max 3 Apps can add in Map Bar! ')
						return stop;
					}
				}
				if($(this).attr('id')=='sortable3'){
					if($('#sortable3 li').length>9){
						$(ui.sender).sortable('cancel');
						alertMeassage(' Max 9 Apps can add in full screen!')
						return stop;
					}
				}
				ui.item.before(ui.item.data('items'));
			},
			stop: function (e, ui) {
				ui.item.siblings('.selected').removeClass('hidden');
				$('.selected').removeClass('selected');
				cluster = [];
				cluster.push($('#sortable2 li:eq(0)').attr('class'));
				cluster.push($('#sortable2 li:eq(1)').attr('class'));
				cluster.push($('#sortable2 li:eq(2)').attr('class'));
				$('#sortable3 li').each(function(){
					cluster.push($(this).attr('class'));
				});
				Settings.save(cluster);
			}
		});
    $("#sortable1, #sortable2, #sortable3").disableSelection();
    $("#sortable1, #sortable2, #sortable3").css('minHeight', $("#sortable1").height() + "px");
		$(document).on('click', '#AlertCloseId', function() {
			$('#overlay').fadeOut();
			$('#AlertPopupId').fadeOut();
		})
});


function alertMeassage(value){
      overlaypopup('AlertPopupId') 
      $('#alert-message').html(value)
}

function setGauges(gauges){
	$.each(gauges.split(','),function(i,gauge){
		if(gauge=='') return;
		if(i>11) return;
		var area = '#sortable2';
		if(i>2) area = '#sortable3';
		$(area).append($('.'+gauge)); //appends the gauge
	});
}