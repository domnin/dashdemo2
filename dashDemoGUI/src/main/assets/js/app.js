     
function overlaypopup(id){
     var doc = $(document);
     var win = $(window);
     var dialog = $('#'+id);
     var noteTop;
     var noteLeft;
     var overlay = $('#overlay');
     if (dialog.height() < win.height()) {
         noteTop = (win.height() - dialog.height()) / 2 + $(window).scrollTop();
         noteLeft = (win.width() - dialog.width()) / 2;         
         // var loginTop = (win.height()-dialog.height()) / 2;
         dialog.css('marginTop', noteTop);
         dialog.css('marginLeft', noteLeft);
         overlay.css('width', doc.width())
         overlay.css('height', doc.height())
         overlay.fadeIn();
         dialog.fadeIn();
     }
}

window.addEventListener('load', function() {     

     /* nagivation click event binding */

     var AddInfoButton = document.getElementById('AddInfoButton');
     var PopupAddNotes = document.getElementById('PopupAddNotes');
     var PopupAddOilchange = document.getElementById('PopupAddOilchange');
     var PopupAddTireBtn = document.getElementById('PopupAddTireBtn');
     var PopupAddBreakBtn = document.getElementById('PopupAddBreakBtn'); 
     var PopupAddFluidBtn = document.getElementById('PopupAddFluidBtn'); 
     var PopupAddBatteryBtn = document.getElementById('PopupAddBatteryBtn'); 
     
      
     
     FastClick.attach(AddInfoButton);
     FastClick.attach(PopupAddNotes);
     FastClick.attach(PopupAddOilchange);
     FastClick.attach(PopupAddTireBtn);
     FastClick.attach(PopupAddBreakBtn); 
     FastClick.attach(PopupAddFluidBtn); 
     FastClick.attach(PopupAddBatteryBtn);     
         

     AddInfoButton.addEventListener('click', function(event) {

         var optionSelected = $('#chooseSelId').find("option:selected");
         var valueSelected = optionSelected.val();
         var textSelected = optionSelected.text();
         var jsLang = valueSelected;
         switch (jsLang) {
             case '1':
                 overlaypopup('oilchangePoppup')
                 break;
             case '2':
                 overlaypopup('notePoppup')
                 break;
             case '3':
                  overlaypopup('tirePoppup');                  
                 break;
             case '4':
                 overlaypopup('breakPoppup');
                 break;
             case '5':
                  overlaypopup('fluidPoppup');
                  break;
             case '6':
                overlaypopup('batteryPoppup');
                break;                       
             default:

         }
     }, false);

PopupAddNotes.addEventListener('click', function(event) {
         $('#overlay').fadeOut();
         $('#notePoppup').fadeOut();
         if ($('#Notes').is(':visible')) {
             alert('Already Added item, Please remove first and try again!');
             return false;
         }
         $('#ContainerBoxId').prepend('<div class="log-blackBox clearfix" id="Notes">\
          <aside class="col-1 yellowbox">\
              <img src="images/note.png"><br/>\
							custom notes\
          </aside>\
          <aside class="col-9">\
              <h2 class="Notes1">Title</h2>\
              <h3 class="Notes2">Description</h3>\
          </aside>\
          <aside class="col-2 center">\
              <input type="button"  class="button-small removeLogbookItem" value="Remove">\
          </aside>\
        </div>');
				Logbooksave();
}, false);
PopupAddOilchange.addEventListener('click', function(event) {
         $('#overlay').fadeOut();
         $('#oilchangePoppup').fadeOut();
         if ($('#Oilchange').is(':visible')) {
             alert('Already Added item, Please remove first and try again!');
             return false;
         };
         $('#ContainerBoxId').prepend('<div class="log-blackBox clearfix" id="Oilchange">\
      <aside class="col-1 yellowbox">\
              <img src="images/engine_oil.png"><br/>\
							oil changes\
          </aside>\
          <aside class="col-3">\
              <h2 class="Oilchange1">Product Name</h2>\
              <h3><span><img src="images/bullet-yellow.png"></span> Due in 90 days</h3>\
          </aside>\
          <aside class="col-6">\
              <div class="progress-conatiner">\
              <h3 id="percentage0" class="percentage">0</h3>\
              <canvas id="progress0" width="423" height="86"></canvas>\
              <input id="slider0" class="slider" type="range" min="0" max="100" value="90" onchange="drawImage(\'progress0\',\'slider0\',0);" />\
              </div>\
          </aside>\
          <aside class="col-2 center">\
              <input type="button"  class="button-small removeLogbookItem" value="Remove">\
          </aside>\
        </div>');
        initadd(0);
				Logbooksave();
}, false);
PopupAddTireBtn.addEventListener('click', function(event) {
         $('#overlay').fadeOut();
         $('#tirePoppup').fadeOut();
         if ($('#TireBtn').is(':visible')) {
             alert('Already Added item, Please remove first and try again!');
             return false;
         };
         $('#ContainerBoxId').prepend('<div class="log-blackBox clearfix" id="TireBtn">\
          <aside class="col-1 yellowbox">\
              <img src="images/wheel.png"><br/>\
							tires\
          </aside>\
          <aside class="col-3">\
              <h2 class="TireBtn1">Product name</h2>\
              <h3><span><img src="images/bullet-red.png"></span> Change R2 Tire</h3>\
          </aside>\
          <aside class="col-6 tire-column">\
              <div class="col-1">\
                <label class="tire-label">F1</label>\
                <span class="tire-indicator">L</span>\
              </div>\
              <div class="col-1">\
                <label class="tire-label">F2</label>\
                <span class="tire-indicator">R</span>\
              </div>\
              <div class="col-1">\
                <label class="tire-label">R1</label>\
                <span class="tire-indicator tire-red">L</span>\
              </div>\
              <div class="col-1">\
                <label class="tire-label">R2</label>\
                <span class="tire-indicator tire-yellow">R</span>\
              </div>\
          </aside>\
          <aside class="col-2 center">\
              <input type="button" class="button-small removeLogbookItem"  value="Remove">\
          </aside>\
        </div>');
				Logbooksave();
}, false);

PopupAddBreakBtn.addEventListener('click', function(event) {
         $('#overlay').fadeOut();
         $('#breakPoppup').fadeOut();
         if ($('#BreakBtn').is(':visible')) {
             alert('Already Added item, Please remove first and try again!');
             return false;
         };
         $('#ContainerBoxId').prepend('<div class="log-blackBox clearfix"  id="BreakBtn" >\
         <aside class="col-1 yellowbox">\
              <img src="images/brakes.png"><br/>\
							brakes\
         </aside>\
         <aside class="col-3">\
              <h2 class="BreakBtn1">Product name</h2>\
              <h3>Due in 90 days</h3>\
         </aside>\
         <aside class="col-6">\
             <div class="progress-conatiner">\
                  <h3 id="percentage1" class="percentage">0</h3>\
                   <canvas id="progress1" width="423" height="86"></canvas>\
                   <input id="slider1" class="slider" type="range" min="0" max="100" value="60"  onchange="drawImage(\'progress1\',\'slider1\',1);"  />\
              </div>\
          </aside>\
          <aside class="col-2 center">\
               <input type="button"  class="button-small removeLogbookItem" value="Remove">\
          </aside>\
          </div>');
					initadd(1);
					Logbooksave();
}, false);

PopupAddFluidBtn.addEventListener('click', function(event) {
         $('#overlay').fadeOut();
         $('#fluidPoppup').fadeOut();
         if ($('#FluidBtn').is(':visible')) {
             alert('Already Added item, Please remove first and try again!');
             return false;
         };
         $('#ContainerBoxId').prepend('<div class="log-blackBox clearfix"  id="FluidBtn" >\
          <aside class="col-1 yellowbox">\
              <img src="images/stearing.png"><br/>\
							power steering fluid\
          </aside>\
          <aside class="col-3">\
              <h2 class="FluidBtn1">Product name</h2>\
              <h3>Due in 90 days or 144 miles</h3>\
          </aside>\
          <aside class="col-6">\
          <div class="progress-conatiner">\
          <h3 id="percentage2" class="percentage">0</h3>\
          <canvas id="progress2" width="423" height="86"></canvas>\
          <input id="slider2" class="slider" type="range" min="0" max="100" value="33"  onchange="drawImage(\'progress2\',\'slider2\',2);" />\
            </div>\
            </aside>\
            <aside class="col-2 center">\
            <input type="button"  class="button-small removeLogbookItem" value="Remove">\
            </aside>\
        </div>');
				initadd(2);
				Logbooksave();
}, false);
PopupAddBatteryBtn.addEventListener('click', function(event) {
         $('#overlay').fadeOut();
         $('#batteryPoppup').fadeOut();
         if ($('#BatteryBtn').is(':visible')) {
             alert('Already Added item, Please remove first and try again!');
             return false;
         };
         $('#ContainerBoxId').prepend('<div class="log-blackBox clearfix" id="BatteryBtn">\
          <aside class="col-1 yellowbox">\
              <br/><img src="images/battery.png"><br/>\
							battery\
          </aside>\
          <aside class="col-3">\
              <h2 class="BatteryBtn1">Product name</h2>\
              <h3>Due in 180   days</h3>\
          </aside>\
          <aside class="col-6">\
          <div class="progress-conatiner">\
              <h3 id="percentage3" class="percentage">0</h3>\
              <canvas id="progress3" width="423" height="86"></canvas>\
                  <input id="slider3" class="slider" type="range" min="0" max="100" value="20"  onchange="drawImage(\'progress3\',\'slider3\',3);" />\
          </div>\
          </aside>\
          <aside class="col-2 center">\
          <input type="button"  class="button-small removeLogbookItem" value="Remove">\
          </aside>\
          </div>');
					initadd(3);
					Logbooksave();
}, false);

}, false);

function Logbooksave() {
	/*
	var logslist = '';
	$('#sortable3 li').each(function(){
	$('.log-blackBox').each(function(){		
	});
	Settings.save(cluster);
	*/
}

function setLogbook(logs){
	$('.log-blackBox').hide();
	$.each(logs.split('└'),function(i,log){
		var logname = log.split('┐')[0];
		var loginf1 = log.split('┐')[1];
		var loginf2 = log.split('┐')[2];
		var loginf3 = log.split('┐')[3];
		eval('PopupAdd'+logname+'.click()');
		$('.'+logname+'1').text(loginf1);
		$('.'+logname+'2').text(loginf2);
		$('.'+logname+'3').text(loginf3);
	});
}