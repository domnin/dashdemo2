var img,
    iHEIGHT = 86,
    iWIDTH = 423,
    canvas = null,
    ctx = null,
    slider = null,
    percent = 0;

function drawBase(ctx, percent) {
    ctx.drawImage(img, 0, 0, iWIDTH, iHEIGHT, 0, 0, iWIDTH, iHEIGHT);
}

function drawProgress(ctx, percent) {
    var x1 = 15, // X position where the progress segment starts
        x2 = 410, // X position where the progress segment ends
        s = slider.value,
        x3 = 0,
        x4 = 144,
        y1 = 63;
    // Calculated x position where the overalyed image should end
    x3 = (x1 + ((x2 - x1) / 100) * s);
    ctx.drawImage(img, 0, iHEIGHT, x3, iHEIGHT, 0, 0, x3, iHEIGHT);
    $('#percentage' + percent).text(s + " %"); // percentage of each progress
}

function drawImage(canvasel, sliderel, percent) {
    // Draw the base image - no progress
    canvas = document.getElementById(canvasel);
    slider = document.getElementById(sliderel);
    ctx = canvas.getContext('2d');
    drawBase(ctx, percent);

    // Draw the progress segment level
    drawProgress(ctx, percent);

}

function initadd(id) {
    var sliderel;
    var canvasel;
    sliderel = "slider" + id;
    canvasel = "progress" + id;
    progressBar(canvasel, sliderel);
}
function progressBar(canvasel, sliderel) {
    percent = canvasel.substring(canvasel.length, 8);
    canvas = document.getElementById(canvasel);
    slider = document.getElementById(sliderel);
    img = new Image();
    ctx = canvas.getContext('2d');
    $(img).load(function(){	drawImage(canvasel,sliderel);	});
    img.src = 'images/progress-tiles.png';
    drawBase(ctx, percent);
    drawProgress(ctx, percent);
}
$(document).ready(function() {
    var removeLogItem;
    var removeApps;
    $(document).on('click', '.removeLogbookItem', function() {
       removeLogItem = $(this).closest('.log-blackBox');
       overlaypopup('removeLogbook')
    })
    $(document).on('click', '.PopupCancel', function() {
        $('#overlay').fadeOut();
        $('.overlay-popup ').fadeOut();
    })

    $(document).on('click', '.removeLogbookBtn', function() {
        $('#overlay').fadeOut();
        $('.overlay-popup ').fadeOut();
        removeLogItem.remove();
    })
    $(document).on('click', '.app-delete', function() {
        removeApps = $(this).closest('li');
        overlaypopup('PopupremoveApps')
    })
    $(document).on('click', '.removeappsBtn', function() {
        $('#overlay').fadeOut();
        $('#PopupremoveApps').fadeOut();
         removeApps.remove();
    })
    $(document).on('click', '.addAppFun', function() {
        removeApps = $(this).closest('li');
         overlaypopup('PopupAddApps')
    })
    $(document).on('click', '.addAppBtn', function() {
       $('#appListId').append('<li>\
                                     <aside>\
                                        <span class="app-icon"><img src="images/pandora-small.png" ></span>\
                                       <label><a href="#">Pandora</a></label>\
                                       <span class="app-delete">x</span>\
                                    </aside>\
                            </li>');                
        $('#overlay').fadeOut();
        $('#PopupAddApps').fadeOut();
    })
});