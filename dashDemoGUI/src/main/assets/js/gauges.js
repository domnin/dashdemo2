//circular gauges drawing
function gaugeCircular(gauge,percentage) {
	var width = $(gauge).width();
	var r = 0.50417*width; //radius
	var a = percentage/100*Math.PI*1.25; //percentage angle transformed in radians
	var aa = a; //preserve original angle size
	var My = Mx = conv = 1; //vars used to realign circle center and convert text value if necessary
	var starting = 0; //smaller gauge value
	percentage>80 ? $(gauge+' svg image').attr('filter','url(#gaugeCircularColor)') : $(gauge+' svg image').removeAttr('filter');	//color for limits
	if(gauge=='.BatteryVoltage') { //corrections for battery gauge
		r = 0.83334*width;	
		a = a/3.2+0.4;
		aa = 	  a-0.4;
		Mx = 0.93;
		My = 0.87;
		conv = 0.1;
		starting = 8;
		33>percentage || percentage>67 ? $(gauge+' svg image').attr('filter','url(#gaugeCircularColor)') : $(gauge+' svg image').removeAttr('filter');	//color for limits
	}
	if(gauge=='.Speed') { //corrections for speed gauge
		r = 0.63334*width;	
		a = a/1.6+5.95;
		aa = 		a-5.95;
		Mx = 0.72;
		My = 0.57;
		conv = 1.2;
		$(gauge+' svg image').removeAttr('filter');
	}
	var a = a-3.5; //correct the initial angle with the image curve start	
	var x  = r*Math.cos(a) + r*Mx; //angle end
	var y  = r*Math.sin(a) + r*My; //angle end 
	a = a-aa; //set original angle
	var sx = r*Math.cos(a) + r*Mx; //angle start
	var sy = r*Math.sin(a) + r*My;	//angle start
	$(gauge+' path').attr('d','M'+r*Mx+','+r*My+' L'+sx+','+sy+' A'+r+','+r+' 0 1,1 '+x+','+y+' z'); //svg path drawing
	$(gauge+' span.text').text(Math.floor(percentage*conv+starting)); //svg number changing
}

//vertical gauges drawing
function gaugeVertical(gauge,percentage) {
	var gaugeHeight = $(gauge).height();
	var maskHeight = (percentage/100*0.45417*gaugeHeight); //converts percentage of mask height for 109px total height
	if(gauge=='.FluidLevel') maskHeight = (percentage/100*0.57084*gaugeHeight); //fluid level gauge has a 137px height mask
	$(gauge+' .mask').css('height',maskHeight); //mask resizing
	percentage<20 ? $(gauge+' .mask').addClass('attention') : $(gauge+' .mask').removeClass('attention');	//color for limits
	if(gauge=='.Temp') percentage>80 ? $(gauge+' .mask').addClass('attention') : $(gauge+' .mask').removeClass('attention'); //color for limits 
};

//initial animations
function initAnimate(gauge){
	var i = 0;
	var move = 'go';	//initial move
	function cycle(gauge){
		if(i>100) move='back'; //reverses the move
		if(move=='back' && i<1) move='goagain';  //reverses again with different move name
		gaugeCircular(gauge,i); //draws 100 steps of the gauge
		gaugeVertical(gauge,i); //draws 100 steps of the gauge
		move=='back' ? i=i-1.5 : i=i+1.5;	//go = increase, back = decrease
		if(move=='goagain' && i>=eval(gauge.split('.')[1])) {  //if reaches/pass the gauge value (preserved in the global var with same name of the gauge class)
			$(gauge).addClass('ready'); //tags the gauge when animation finishes so the settings functions can run without affecting animation
		} else { 
			setTimeout(function(){cycle(gauge)},1); //restarts the cycle with a little delay for animation
		}
	}
	cycle(gauge);
}

//settings functions
function setRPM(value) {
	RPM = value/0.1;
	gaugeCircular('.RPM',RPM) 
}
function setBatteryVoltage(value) {
	BatteryVoltage = (value-8)/0.1;
	gaugeCircular('.BatteryVoltage',BatteryVoltage) 
}
function setSpeed(value) {
	Speed = value/1.2;
	gaugeCircular('.Speed',Speed) 
}
function setTorque(value) {
	Torque = value/1.2;
	gaugeCircular('.Torque',Torque) 
}
function setTemp(value) {
	Temp = value;
	gaugeVertical('.Temp',Temp) 
}
function setFuel (value) {
	Fuel = value;
	gaugeVertical('.Fuel',Fuel) 
}
function setEngineOil(value) {
	EngineOil = value;
	gaugeVertical('.EngineOil',EngineOil) 
}
function setFluidLevel(value) {
	FluidLevel = value;
	gaugeVertical('.FluidLevel',FluidLevel) 
}

//door status
function setDoorStatus(door_lf,door_rf,door_lb,door_rb)	{ //door_lf = left front door //door_rb = right back door 
	$('.doors span').removeClass('open'); //refresh status 
	if(door_lf==1) $('.DoorStatus .left_front ').addClass('open');
	if(door_rf==1) $('.DoorStatus .right_front').addClass('open');
	if(door_lb==1) $('.DoorStatus .left_back  ').addClass('open');
	if(door_rb==1) $('.DoorStatus .right_back ').addClass('open');
}

//tires
function setTires(tire_lf,psi_lf,temp_lf,tire_rf,psi_rf,temp_rf,tire_lb,psi_lb,temp_lb,tire_rb,psi_rb,temp_rb) { //lf = left front tire //rb = right back tire
	$('.Tires .left_front ').html('<em>'+psi_lf+'</em>'+temp_lf+'°');
	$('.Tires .right_front').html('<em>'+psi_rf+'</em>'+temp_rf+'°');
	$('.Tires .left_back  ').html('<em>'+psi_lb+'</em>'+temp_lb+'°');
	$('.Tires .right_back ').html('<em>'+psi_rb+'</em>'+temp_rb+'°');
	$('.Tires span').removeClass('open'); //refresh status 
	if(tire_lf==1) $('.Tires .left_front' ).addClass('open');
	if(tire_rf==1) $('.Tires .right_front').addClass('open');
	if(tire_lb==1) $('.Tires .left_back'  ).addClass('open');
	if(tire_rb==1) $('.Tires .right_back ').addClass('open');
}

//odometer
function setOdometer(value) {
	var str = ''+value; //number to string
	var pad = '0000000'; //leading zeros
	var ans = pad.substring(0, pad.length - str.length) + str; //small number to number with leading zeros
	var i = 0;
	$('.Odometer span').each(function(){
		$(this).text(ans.charAt(i)); //text each number in a span for better grid
		i++
	});
}

//Outside Temp
function setOutsideTemp(value) {
	$('.OutsideTemp span').text(value+'°F');
}
//Intake Temp
function setIntakeTemp(value) {
	$('.IntakeTemp span').text(value+'°F');
}
//Catalyst Temp
function setCatalystTemp(value) {
	$('.CatalystTemp span').text(value+'°F');
}

//gear
function setGear(value) {
	$('.Gear span').hide();
	$('.Gear span.Gear'+value).show();
	$('.Gear span.GearActive').show().text(value);
}

//auto gear
function setAutoGear(value) {
	$('.AutoGear span').hide();
	$('.AutoGear span.AutoGear'+value).show();
	$('.AutoGear span.AutoGearActive').show().text(value);
}

function setGPSHeading(value) {
	if( (0<=value && value<23) || 338<=value) $('.GPSHeadingLetter').text('N');
	if( 23<=value && value<68) $('.GPSHeadingLetter').text('NE');
	if( 68<=value && value<113) $('.GPSHeadingLetter').text('E');
	if(113<=value && value<158) $('.GPSHeadingLetter').text('SE');
	if(158<=value && value<203) $('.GPSHeadingLetter').text('S');
	if(203<=value && value<248) $('.GPSHeadingLetter').text('SW');
	if(248<=value && value<293) $('.GPSHeadingLetter').text('W');
	if(293<=value && value<338) $('.GPSHeadingLetter').text('NW');
	$('.GPSHeadingPin').css({
		'-webkit-transform' : 'rotate('+ value +'deg)',
    '-moz-transform' : 'rotate('+ value +'deg)',
    '-ms-transform' : 'rotate('+ value +'deg)',
    'transform' : 'rotate('+ value +'deg)'});			
}

//sorting the gauges
function sortgauge(sortcont,eachgauge){
	$(sortcont).sortable({
			connectWith: sortcont,
			stop: function (event, ui) {
				 $(sortcont).each(function () {
							result = "";
						 // alert($(this).sortable("toArray"));
							$(this).find(eachgauge).each(function () {
									result += $(this).attr('id') + ",";
							});
					});
			}
	});
}

//responsive widths and font-size
function responsive() {
	$('.gauges>div').height($('.gauges>div:last').width());
	$('.gauges').css('font-size',$('.gauges>div:last').width()/21.5); //12px at max width
	$('.dashbrd>a').css('font-size',$('#dashbrdcontr').width()/15); 	
}

//initializing gauges
function setGauges(gauges){
	$.each(gauges.split(','),function(i,gauge){
		if(gauge=='') return;
		if($('.gaugesnav').length>0 && i>2) return;
		if(gauge.split('_').length<2) { //if gauge is not 2 gauges merged
			window[gauge] = 0; //creates global vars for future value storage
			$('.gauges').append($('.'+gauge).show()); //appends the gauge
		} else { //if they are merged, repeat all above for each one
			var gauge0 = gauge.split('_')[0];
			var gauge1 = gauge.split('_')[1];
			window[gauge0] = 0;
			window[gauge1] = 0;
			$('.gauges').append('<div class="gaugeMerged" style="display:block"></div>'); //appends the gauge
			$('.gaugeMerged:last').append($('.'+gauge0).show()); //appends the gauge
			$('.gaugeMerged:last').append($('.'+gauge1).show()); //appends the gauge
		}
	});
	setRPM(9);
	setBatteryVoltage(14);
	setSpeed(30);
	setTorque(9);
	setFuel(19);
	setTemp(50);
	setEngineOil(50);
	setFluidLevel(50);
	setDoorStatus(1,0,0,0); 
	setTires(0,34.7,28,0,35.0,29,1,29.5,25,0,34.6,28); 
	setOdometer(96035); //7 numbers max //creates leading zeroes 
	setOutsideTemp(100);
	setIntakeTemp(100);
	setCatalystTemp(100);
	setGear('R');
	setAutoGear('R');
	setGPSHeading(200);
	responsive();
}

$(window).resize(function(){responsive()});