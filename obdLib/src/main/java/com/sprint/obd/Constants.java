package com.sprint.obd;

/**
 * Created by dyd0912 on 1/21/2015.
 */
public final class Constants {

    public static final String BLUETOOTH_LIST_KEY = "bluetooth_list_preference";
    public static final String INIT_COMMAND_KEY = "init_command_preference";
    public static final String RPM_COMMAND_KEY = "rpm_command_preference";




    public static final String UPLOAD_URL_KEY = "upload_url_preference";
    public static final String UPLOAD_DATA_KEY = "upload_data_preference";
    public static final String UPDATE_PERIOD_KEY = "update_period_preference";
    public static final String VEHICLE_ID_KEY = "vehicle_id_preference";
    public static final String ENGINE_DISPLACEMENT_KEY = "engine_displacement_preference";
    public static final String VOLUMETRIC_EFFICIENCY_KEY = "volumetric_efficiency_preference";
    public static final String IMPERIAL_UNITS_KEY = "imperial_units_preference";
    public static final String COMMANDS_SCREEN_KEY = "obd_commands_screen";
    public static final String ENABLE_GPS_KEY = "enable_gps_preference";
    public static final String MAX_FUEL_ECON_KEY = "max_fuel_econ_preference";
    public static final String CONFIG_READER_KEY = "reader_config_preference";
}
