package com.sprint.obd;

import android.content.SharedPreferences;

import java.util.ArrayList;

import pt.lighthouselabs.obd.commands.ObdCommand;
import pt.lighthouselabs.obd.reader.config.ObdConfig;

/**
 * Created by dyd0912 on 1/21/2015.
 */
public class Config {
    private SharedPreferences prefs;

    public Config(SharedPreferences prefs) {
        this.prefs = prefs;
    }

    /**
     * @return
     */
    public ArrayList<ObdCommand> getObdCommands() {
        ArrayList<ObdCommand> cmds = ObdConfig.getCommands();
        ArrayList<ObdCommand> ucmds = new ArrayList<ObdCommand>();
        for (int i = 0; i < cmds.size(); i++) {
            ObdCommand cmd = cmds.get(i);
            boolean selected = prefs.getBoolean(cmd.getName(), true);
            if (selected)
                ucmds.add(cmd);
        }
        return ucmds;
    }

    /**
     * Return configured value or default
     * @return
     */
    public String getInitCommands() {
        return prefs.getString(Constants.INIT_COMMAND_KEY, "ATE0,ATL0,ATST62,ATSP6,ATH0");
    }

}
