package pt.lighthouselabs.obd.commands;

import java.math.BigInteger;

/**
 * Created by dyd0912 on 2/2/2015.
 */
public class ObdScriptedCommand extends ObdCommand {
    private String id;  // command ID
    private String name;  // command Name
    private String prefix;  // result prefix like 410C
    private BigInteger _mask;
    private int shift;

    private String formula;
    public ObdScriptedCommand(String cmd) {
        super(cmd);
    }

    @Override
    protected void performCalculations() {

    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getFormattedResult() {
        return null;
    }
}
