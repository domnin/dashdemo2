/**
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package pt.lighthouselabs.obd.commands;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import pt.lighthouselabs.obd.exceptions.BusInitException;
import pt.lighthouselabs.obd.exceptions.MisunderstoodCommandException;
import pt.lighthouselabs.obd.exceptions.NoDataException;
import pt.lighthouselabs.obd.exceptions.NonNumericResponseException;
import pt.lighthouselabs.obd.exceptions.ObdResponseException;
import pt.lighthouselabs.obd.exceptions.StoppedException;
import pt.lighthouselabs.obd.exceptions.UnableToConnectException;
import pt.lighthouselabs.obd.exceptions.UnknownObdErrorException;
import pt.lighthouselabs.obd.reader.util.Logger;

//import pt.lighthouselabs.obd.reader.activity.MainActivity;

/**
 * Base OBD command.
 */
public abstract class ObdCommand {

    protected ArrayList<Integer> buffer = null;
    protected String cmd = null;
    protected boolean useImperialUnits = false;
    protected String rawData = null;


    protected boolean isMoreData;

    /**
     * Error classes to be tested in order
     */
    private Class[] ERROR_CLASSES = { // CHECK FOR DIFFERENT ERROR RESPONSES
            UnableToConnectException.class,
            BusInitException.class,
            MisunderstoodCommandException.class,
            NoDataException.class,
            StoppedException.class,
            UnknownObdErrorException.class
    };

    /**
     * Default ctor to use
     *
     * @param command the command to send
     */
    public ObdCommand(String command) {
        this.cmd = command;
        this.buffer = new ArrayList<Integer>();
    }

    /**
     * Prevent empty instantiation
     */
    private ObdCommand() {
    }

    /**
     * Copy ctor.
     *
     * @param other the ObdCommand to copy.
     */
    public ObdCommand(ObdCommand other) {
        this(other.cmd);
    }

    /**
     * Sends the OBD-II request and deals with the response.
     * <p/>
     * This method CAN be overridden in fake commands.
     */
    public void run(InputStream in, OutputStream out) throws IOException,
            InterruptedException {
        sendCommand(out); // send and read reply
        readResult(in);
    }

    /**
     * Continue reading for MT
     */
    public void runMT(InputStream in) throws IOException,
            InterruptedException {
        if (isMoreData()) {
            readResult(in);
        }
    }

    static String lastCommand = "";

    /**
     * Sends the OBD-II request.
     * <p/>
     * This method may be overriden in subclasses, such as ObMultiCommand or
     * TroubleCodesObdCommand.
     *
     * @param out The output stream.
     */
    protected void sendCommand(OutputStream out) throws IOException,
            InterruptedException {
        // add the carriage return char
        if (!cmd.endsWith("\r")) {
            cmd += "\r";
        }
        // write to OutputStream (i.e.: a BluetoothSocket)
        if (cmd.equals(lastCommand)) {
            out.write("\r".getBytes());
            Logger.d("Sent:" + "..repeat command.." + cmd + "\r");
        } else {
            Logger.d("Sent:" + cmd + "\r");
            out.write(cmd.getBytes());
        }
        lastCommand = cmd;
        out.flush();

    /*
     * HACK GOLDEN HAMMER ahead!!
     * 
     * Due to the time that some systems may take to respond, let's give it
     * 200ms.
     */
        Thread.sleep(300);
    }

    /**
     * Resends this command.
     */
    protected void resendCommand(OutputStream out) throws IOException,
            InterruptedException {
        out.write("\r".getBytes());
        out.flush();
    }

    /**
     * Reads the OBD-II response.
     * <p/>
     * This method may be overriden in subclasses, such as ObdMultiCommand.
     */
    protected void readResult(InputStream in) throws IOException { //
        readRawData(in);
        checkForErrors();
        fillBuffer();
        performCalculations();
    }

    /**
     * This method exists so that for each command, there must be a method that is
     * called only once to perform calculations.
     */
    protected abstract void performCalculations();

    /**
     *
     */
    protected void fillBuffer() {
        rawData = rawData.replaceAll("\\s", "");

        if (!rawData.matches("([0-9A-F]{2})+")) { // allow hex only
            throw new NonNumericResponseException(rawData);
        }

        // read string each two chars
        buffer.clear();
        int begin = 0;
        int end = 2;
        while (end <= rawData.length()) {
            buffer.add(Integer.decode("0x" + rawData.substring(begin, end)));
            begin = end;
            end += 2;
        }
    }

    protected void readRawData(InputStream in) throws IOException {
        byte b = 0;
        StringBuilder res = new StringBuilder();

        // read until '>' arrives or until too long
        isMoreData = false;
        while ((char) (b = (byte) in.read()) != '>') {
            res.append((char) b);
            if (res.length() > 256 && b == 13) { // break at new line
                isMoreData = true;
                break;
            }

        }

    /*
     * Imagine the following response 41 0c 00 0d.
     * 
     * ELM sends strings!! So, ELM puts spaces between each "byte". And pay
     * attention to the fact that I've put the word byte in quotes, because 41
     * is actually TWO bytes (two chars) in the socket. So, we must do some more
     * processing..
     */
        rawData = res.toString().trim();
        rawData = rawData.replace("?", "");
//        Log.d(MainActivity.TAG, "Raw Data for " + getName() + ":" + rawData);
        Logger.d("Received for " + getName() + ":" + rawData + "\r");
    /*
     * Data may have echo or informative text like "INIT BUS..." or similar.
     * The response ends with two carriage return characters. So we need to take
     * everything from the last carriage return before those two (trimmed above).
     */
        //TODO
        //   rawData = rawData.substring(rawData.lastIndexOf(13) + 1);
    }

    void checkForErrors() {
        for (Class<? extends ObdResponseException> errorClass : ERROR_CLASSES) { // check for different errors
            ObdResponseException messageError;

            try {
                messageError = errorClass.newInstance();
                messageError.setCommand(this.cmd);
            } catch (InstantiationException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }

            if (messageError.isError(rawData)) {    //
                throw messageError; // throw error if message found
            }
        }
    }

    /**
     * @return the raw command response in string representation.
     */
    public String getResult() {
        return rawData;
    }

    /**
     * remove spaces from raw result
     *
     * @return
     */
    public String getTrimmedResult() {
        if (getResult() != null) {
            if (getResult().contains("\n")) {
                // if double line return only first
                return getResult().substring(0, getResult().indexOf("\n")).replace(" ", "");
            }
            if (getResult().contains("\r")) {
                // if double line return only first
                return getResult().substring(0, getResult().indexOf("\r")).replace(" ", "");
            }

            return getResult().replace(" ", "");
        }
        return "";
    }

    /**
     * @return a formatted command response in string representation.
     */
    public abstract String getFormattedResult();

    /**
     * @return a list of integers
     */
    protected ArrayList<Integer> getBuffer() {
        return buffer;
    }

    /**
     * @return true if imperial units are used, or false otherwise
     */
    public boolean useImperialUnits() {
        return useImperialUnits;
    }

    /**
     * Set to 'true' if you want to use imperial units, false otherwise. By
     * default this value is set to 'false'.
     *
     * @param isImperial
     */
    public void useImperialUnits(boolean isImperial) {
        this.useImperialUnits = isImperial;
    }

    /**
     * @return the OBD command name.
     */
    public abstract String getName();

    public boolean isMoreData() {
        return isMoreData;
    }

    public String getRawCommand() {
        return cmd;
    }

}
