package pt.lighthouselabs.obd.reader;

import pt.lighthouselabs.obd.reader.io.ObdCommandJob;

/**
 * TODO put description
 */
public interface ObdProgressListener {
    static int BT_STATUS_CONNECTED = 2;
    static int BT_STATUS_CONNECTING = 1;
    static int BT_STATUS_DISCONNECTED = 0;
    static int BT_STATUS_ERROR = -1;

    static int CAR_STATUS_CONNECTED = 2;
    static int CAR_STATUS_CONNECTING = 1;
    static int CAR_STATUS_DISCONNECTED = 0;
    static int CAR_STATUS_ERROR = -1;


    void stateUpdate(final ObdCommandJob job);

    void stateUpdateMT(String rawResult);

    void addTableRow(String key, String val);

    void btConnectionStatusChange(int status);

    void carConnectionStatusChange(int status);

}