package pt.lighthouselabs.obd.reader.io;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.inject.Inject;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

//import pt.lighthouselabs.obd.reader.activity.MainActivity;
import pt.lighthouselabs.obd.reader.ObdProgressListener;
import roboguice.service.RoboService;


public abstract class AbstractGatewayService extends RoboService {
    private static final String TAG = AbstractGatewayService.class.getName();
    public static final int NOTIFICATION_ID = 1;
    @Inject
    protected NotificationManager notificationManager;

    protected Context ctx;
    protected ObdProgressListener connectionStatusListener;

    protected static boolean isRunning = false; // TODO ??
    private final IBinder binder = new AbstractGatewayServiceBinder();
    protected static Boolean isQueueRunning = false;// TODO ??
    protected Long queueCounter = 0L;
    protected BlockingQueue<ObdCommandJob> jobsQueue = new LinkedBlockingQueue<ObdCommandJob>();

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Creating service..");
        Log.d(TAG, "Service created.");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Destroying service...");
        if (notificationManager != null) {
            notificationManager.cancel(NOTIFICATION_ID);
        }
        Log.d(TAG, "Service destroyed.");

    }

    public boolean isRunning() {
        return isRunning;
    }


    public class AbstractGatewayServiceBinder extends Binder {
        public AbstractGatewayService getService() {
            return AbstractGatewayService.this;
        }
    }

    /**
     * Queue a sequence command which may include config, like protocol change
     *
     * @param jobs
     */
    public synchronized void queueJobs(List<ObdCommandJob> jobs, boolean canSkip) {
        if (jobs != null) {
            for (ObdCommandJob job : jobs) {
                queueJob(job, canSkip);
            }
        }
    }

    /*
     * Changed to private and exposed queueJobs instead to  queue sequences
     * to prevent interference between threads
     * This method will add a job to the queue while setting its ID to the
     * internal queue counter.
     *
     * @param job the job to queue.
     */
    private void queueJob(ObdCommandJob job, boolean canSkip) {
        Log.d(TAG, "queueSize=" + jobsQueue.size());
        if (canSkip && jobsQueue.size() > 10) {
            job.setState(ObdCommandJob.ObdCommandJobState.SKIPPED);
            Log.d(TAG, "Skipping command " + job.getCommand().getRawCommand());
            return;
        }
        queueCounter++;
//        Log.d(TAG, "Adding job[" + queueCounter + "] to queue.." + job.getCommand().getName());

        job.setId(queueCounter);
        try {
            jobsQueue.put(job);
//            Log.d(TAG, "Job queued successfully.");
        } catch (InterruptedException e) {
            job.setState(ObdCommandJob.ObdCommandJobState.QUEUE_ERROR);
            Log.e(TAG, "Failed to queue job.");
        }

        if (!isQueueRunning) {
            // Run the executeQueue in a different thread to lighten the UI thread
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    executeQueue();
                }
            });

            t.start();
        }
    }

    /**
     * Show a notification while this service is running.
     */
    protected void showNotification(String contentTitle, String contentText, int icon, boolean ongoing, boolean notify, boolean vibrate) {
        final PendingIntent contentIntent = null;// PendingIntent.getActivity(ctx, 0, new Intent(ctx, MainActivity.class), 0);
        final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(ctx);
        notificationBuilder.setContentTitle(contentTitle)
                .setContentText(contentText).setSmallIcon(icon)
                .setContentIntent(contentIntent)
                .setWhen(System.currentTimeMillis());
        // can cancel?
        if (ongoing) {
            notificationBuilder.setOngoing(true);
        } else {
            notificationBuilder.setAutoCancel(true);
        }
        if (vibrate) {
            notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        }
        if (notify) {
            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.getNotification());
        }
    }

    /*
    @deprecated
     */
    public void setContext(Context c) {
        ctx = c;
    }

    public void setConnectionListener(ObdProgressListener l) {
        connectionStatusListener = l;
    }

    abstract protected void executeQueue();

    abstract public void startService();

    abstract public void stopService();
}
