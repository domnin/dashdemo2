package pt.lighthouselabs.obd.reader.util;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
    static final String LOG_TAG = "ODB2";
    private static boolean IS_LOGGING_ENABLED = true;
    static FileWriter writer = null;
    static final String LOG_DIR = "/logs/odb2";
    static boolean canWrite = true;
    private static SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

    static synchronized Writer getWriter() {

        if (canWrite) {
            if (writer == null) {
                Log.d(LOG_TAG, "getWriter");
                File root = Environment.getExternalStorageDirectory();

                Log.d(LOG_TAG, root + "");

                if (root.canWrite()) {
                    Log.d(LOG_TAG, "canWrite=true");
                    File dir = new File(root, LOG_DIR);
                    dir.mkdirs();
                    File file = new File(root, LOG_DIR + "/OBD_LOG_"
                            + System.currentTimeMillis() + ".txt");

                    Log.d(LOG_TAG, file.toString());
                    try {
                        writer = new FileWriter(file);
                    } catch (IOException e) {
                        e("Cannot open file", e);
                    }
                } else {
                    Log.d(LOG_TAG, "canWrite=false");
                    canWrite = false;
                }

            }
        }
        return writer;
    }

    public static void writeFile(String s) {
        Writer w = getWriter();
        if (w != null) {
            try {
                w.write(format.format(new Date()) + " " + s + "\r\n");
                w.flush();
            } catch (IOException e) {
            }
        }
    }

    public static synchronized void closeLog() {
        if (writer != null) {
            try {
                writer.close();
                writer = null;
            } catch (Exception e) {
            }
        }
    }

    public static void d(String msg) {
        if (IS_LOGGING_ENABLED) {
            if (msg == null)
                msg = "null";
            Log.d(LOG_TAG, msg);
            writeFile(msg);
        }
    }

    public static void w(String msg) {
        if (IS_LOGGING_ENABLED) {
            if (msg == null)
                msg = "null";
            Log.w(LOG_TAG, msg);
            writeFile(msg);
        }
    }

    public static void i(String msg) {
        if (IS_LOGGING_ENABLED) {
            if (msg == null)
                msg = "null";
            Log.i(LOG_TAG, msg);
            writeFile(msg);
        }
    }

    public static void e(String msg, Exception e) {
        if (IS_LOGGING_ENABLED) {
            if (msg == null)
                msg = "null";
            Log.e(LOG_TAG, msg, e);
            writeFile(msg);

        }
    }

    public static void e(String msg) {
        if (IS_LOGGING_ENABLED) {
            if (msg == null)
                msg = "null";
            Log.e(LOG_TAG, msg);
            writeFile(msg);
        }
    }

    public void closeLogFile() {
        if (writer != null) {
            try {
                writer.close();
            } catch (IOException e) {
                canWrite = false;
                e("error closing writer", e);
            }
        }
    }
}
