package pt.lighthouselabs.obd.reader.util;

import java.util.ArrayList;
import java.util.List;

import pt.lighthouselabs.obd.commands.protocol.OdbRawCommand;
import pt.lighthouselabs.obd.reader.ObdProgressListener;
import pt.lighthouselabs.obd.reader.io.ObdCommandJob;

/**
 * Created by dyd0912 on 1/29/2015.
 */
public final class ObdHelper {
    /**
     * Sake list of comma separated hex OBD command and convert to list of job objects
     * Example: "010C,010D"
     *
     * @param commands
     * @return
     */
    public static List<ObdCommandJob> buildJobList(String commands, ObdProgressListener listener) {

        ArrayList<ObdCommandJob> jobs = new ArrayList<ObdCommandJob>();
        if (commands != null) {
            if (commands.contains(",")) {
                String[] cArray = commands.split(",");
                for (String cmd : cArray) {
                    if (cmd.length() != 0) {
                        jobs.add(new ObdCommandJob(new OdbRawCommand(cmd), listener));
                    }
                }
            } else {
                if (commands.length() != 0) {
                    jobs.add(new ObdCommandJob(new OdbRawCommand(commands), listener));
                }

            }

        }

        return jobs;
    }

}
